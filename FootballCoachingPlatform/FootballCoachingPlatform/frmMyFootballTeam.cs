﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using FootballCoachingPlatform.Model;

namespace FootballCoachingPlatform
{
    public partial class frmMyFootballTeam : Form
    {
        public frmMyFootballTeam()
        {
            InitializeComponent();

            try
            {
                imgTeamLogo.Image = Image.FromFile(Team.imagePath);
            } catch (Exception)
            {
                imgTeamLogo.Image = Properties.Resources.onbekend;
            }

            try
            {
                imgCoach.Image = Image.FromFile(Coach.imagePath);
            }
            catch (Exception)
            {
                imgCoach.Image = Properties.Resources.onbekend;
            }       

            lblTeamName.Text = Team.name;
            lblUsername.Text = Coach.username;
        }

        private void imgMyTeam_Click(object sender, EventArgs e)
        {
            new frmMyTeam().ShowDialog();
        }

        private void imgTraining_Click(object sender, EventArgs e)
        {
            new frmTraining().ShowDialog();
        }

        private void imgMatchday_Click(object sender, EventArgs e)
        {
            new frmMatchday().ShowDialog();
        }
    }
}
