﻿namespace FootballCoachingPlatform
{
    partial class frmTrainingHistory
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnPrevious = new System.Windows.Forms.Button();
            this.btnNext = new System.Windows.Forms.Button();
            this.imgCoach = new System.Windows.Forms.PictureBox();
            this.lblTeamName = new System.Windows.Forms.Label();
            this.lblUsername = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.imgTeam = new System.Windows.Forms.PictureBox();
            this.btnAdd = new System.Windows.Forms.Button();
            this.btnDelete = new System.Windows.Forms.Button();
            this.btnEdit = new System.Windows.Forms.Button();
            this.dtpDate = new System.Windows.Forms.DateTimePicker();
            this.lblPoints3 = new System.Windows.Forms.Label();
            this.lblPoints2 = new System.Windows.Forms.Label();
            this.lblPoints1 = new System.Windows.Forms.Label();
            this.lblEffort = new System.Windows.Forms.Label();
            this.lblShape = new System.Windows.Forms.Label();
            this.lblAttitude = new System.Windows.Forms.Label();
            this.txtEffort = new System.Windows.Forms.TextBox();
            this.txtAttitude = new System.Windows.Forms.TextBox();
            this.txtShape = new System.Windows.Forms.TextBox();
            this.lblComment = new System.Windows.Forms.Label();
            this.txtComment = new System.Windows.Forms.TextBox();
            this.lblPlayerName = new System.Windows.Forms.Label();
            this.imgPlayer = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.imgCoach)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgTeam)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgPlayer)).BeginInit();
            this.SuspendLayout();
            // 
            // btnPrevious
            // 
            this.btnPrevious.Location = new System.Drawing.Point(41, 386);
            this.btnPrevious.Name = "btnPrevious";
            this.btnPrevious.Size = new System.Drawing.Size(75, 23);
            this.btnPrevious.TabIndex = 0;
            this.btnPrevious.Text = "Vorige";
            this.btnPrevious.UseVisualStyleBackColor = true;
            this.btnPrevious.Click += new System.EventHandler(this.btnPrevious_Click);
            // 
            // btnNext
            // 
            this.btnNext.Location = new System.Drawing.Point(691, 386);
            this.btnNext.Name = "btnNext";
            this.btnNext.Size = new System.Drawing.Size(75, 23);
            this.btnNext.TabIndex = 1;
            this.btnNext.Text = "Volgende";
            this.btnNext.UseVisualStyleBackColor = true;
            this.btnNext.Click += new System.EventHandler(this.btnNext_Click);
            // 
            // imgCoach
            // 
            this.imgCoach.Location = new System.Drawing.Point(41, 25);
            this.imgCoach.Name = "imgCoach";
            this.imgCoach.Size = new System.Drawing.Size(99, 80);
            this.imgCoach.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.imgCoach.TabIndex = 10;
            this.imgCoach.TabStop = false;
            // 
            // lblTeamName
            // 
            this.lblTeamName.AutoSize = true;
            this.lblTeamName.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblTeamName.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTeamName.Location = new System.Drawing.Point(155, 25);
            this.lblTeamName.Name = "lblTeamName";
            this.lblTeamName.Size = new System.Drawing.Size(135, 27);
            this.lblTeamName.TabIndex = 11;
            this.lblTeamName.Text = "<Clubnaam>";
            // 
            // lblUsername
            // 
            this.lblUsername.AutoSize = true;
            this.lblUsername.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblUsername.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUsername.Location = new System.Drawing.Point(155, 78);
            this.lblUsername.Name = "lblUsername";
            this.lblUsername.Size = new System.Drawing.Size(196, 27);
            this.lblUsername.TabIndex = 12;
            this.lblUsername.Text = "<Gebruikersnaam>";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(338, 25);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(323, 35);
            this.label6.TabIndex = 13;
            this.label6.Text = "Training geschiedenis";
            // 
            // imgTeam
            // 
            this.imgTeam.Location = new System.Drawing.Point(667, 25);
            this.imgTeam.Name = "imgTeam";
            this.imgTeam.Size = new System.Drawing.Size(99, 80);
            this.imgTeam.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.imgTeam.TabIndex = 21;
            this.imgTeam.TabStop = false;
            // 
            // btnAdd
            // 
            this.btnAdd.BackColor = System.Drawing.Color.DarkOliveGreen;
            this.btnAdd.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAdd.ForeColor = System.Drawing.Color.PaleTurquoise;
            this.btnAdd.Location = new System.Drawing.Point(633, 131);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(133, 56);
            this.btnAdd.TabIndex = 22;
            this.btnAdd.Text = "Voeg toe";
            this.btnAdd.UseVisualStyleBackColor = false;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.BackColor = System.Drawing.Color.Red;
            this.btnDelete.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDelete.ForeColor = System.Drawing.Color.PaleTurquoise;
            this.btnDelete.Location = new System.Drawing.Point(633, 296);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(133, 56);
            this.btnDelete.TabIndex = 23;
            this.btnDelete.Text = "Verwijder";
            this.btnDelete.UseVisualStyleBackColor = false;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnEdit
            // 
            this.btnEdit.BackColor = System.Drawing.Color.DarkOrange;
            this.btnEdit.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEdit.ForeColor = System.Drawing.Color.PaleTurquoise;
            this.btnEdit.Location = new System.Drawing.Point(633, 215);
            this.btnEdit.Name = "btnEdit";
            this.btnEdit.Size = new System.Drawing.Size(133, 56);
            this.btnEdit.TabIndex = 24;
            this.btnEdit.Text = "Bewerk";
            this.btnEdit.UseVisualStyleBackColor = false;
            this.btnEdit.Click += new System.EventHandler(this.btnEdit_Click);
            // 
            // dtpDate
            // 
            this.dtpDate.Location = new System.Drawing.Point(44, 131);
            this.dtpDate.Name = "dtpDate";
            this.dtpDate.Size = new System.Drawing.Size(215, 20);
            this.dtpDate.TabIndex = 39;
            this.dtpDate.ValueChanged += new System.EventHandler(this.dtpDate_ValueChanged);
            // 
            // lblPoints3
            // 
            this.lblPoints3.AutoSize = true;
            this.lblPoints3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPoints3.Location = new System.Drawing.Point(287, 267);
            this.lblPoints3.Name = "lblPoints3";
            this.lblPoints3.Size = new System.Drawing.Size(31, 20);
            this.lblPoints3.TabIndex = 38;
            this.lblPoints3.Text = "/10";
            // 
            // lblPoints2
            // 
            this.lblPoints2.AutoSize = true;
            this.lblPoints2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPoints2.Location = new System.Drawing.Point(296, 310);
            this.lblPoints2.Name = "lblPoints2";
            this.lblPoints2.Size = new System.Drawing.Size(31, 20);
            this.lblPoints2.TabIndex = 37;
            this.lblPoints2.Text = "/10";
            // 
            // lblPoints1
            // 
            this.lblPoints1.AutoSize = true;
            this.lblPoints1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPoints1.Location = new System.Drawing.Point(309, 223);
            this.lblPoints1.Name = "lblPoints1";
            this.lblPoints1.Size = new System.Drawing.Size(31, 20);
            this.lblPoints1.TabIndex = 36;
            this.lblPoints1.Text = "/10";
            // 
            // lblEffort
            // 
            this.lblEffort.AutoSize = true;
            this.lblEffort.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEffort.Location = new System.Drawing.Point(182, 310);
            this.lblEffort.Name = "lblEffort";
            this.lblEffort.Size = new System.Drawing.Size(50, 20);
            this.lblEffort.TabIndex = 35;
            this.lblEffort.Text = "Inzet";
            // 
            // lblShape
            // 
            this.lblShape.AutoSize = true;
            this.lblShape.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblShape.Location = new System.Drawing.Point(182, 267);
            this.lblShape.Name = "lblShape";
            this.lblShape.Size = new System.Drawing.Size(51, 20);
            this.lblShape.TabIndex = 34;
            this.lblShape.Text = "Vorm";
            // 
            // lblAttitude
            // 
            this.lblAttitude.AutoSize = true;
            this.lblAttitude.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAttitude.Location = new System.Drawing.Point(182, 223);
            this.lblAttitude.Name = "lblAttitude";
            this.lblAttitude.Size = new System.Drawing.Size(76, 20);
            this.lblAttitude.TabIndex = 33;
            this.lblAttitude.Text = "Houding";
            // 
            // txtEffort
            // 
            this.txtEffort.Location = new System.Drawing.Point(238, 310);
            this.txtEffort.Name = "txtEffort";
            this.txtEffort.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtEffort.Size = new System.Drawing.Size(52, 20);
            this.txtEffort.TabIndex = 32;
            // 
            // txtAttitude
            // 
            this.txtAttitude.Location = new System.Drawing.Point(264, 223);
            this.txtAttitude.Name = "txtAttitude";
            this.txtAttitude.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtAttitude.Size = new System.Drawing.Size(39, 20);
            this.txtAttitude.TabIndex = 31;
            // 
            // txtShape
            // 
            this.txtShape.Location = new System.Drawing.Point(238, 267);
            this.txtShape.Name = "txtShape";
            this.txtShape.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtShape.Size = new System.Drawing.Size(43, 20);
            this.txtShape.TabIndex = 30;
            // 
            // lblComment
            // 
            this.lblComment.AutoSize = true;
            this.lblComment.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblComment.Location = new System.Drawing.Point(468, 219);
            this.lblComment.Name = "lblComment";
            this.lblComment.Size = new System.Drawing.Size(125, 25);
            this.lblComment.TabIndex = 29;
            this.lblComment.Text = "Opmerking";
            // 
            // txtComment
            // 
            this.txtComment.Location = new System.Drawing.Point(368, 247);
            this.txtComment.Multiline = true;
            this.txtComment.Name = "txtComment";
            this.txtComment.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtComment.Size = new System.Drawing.Size(225, 86);
            this.txtComment.TabIndex = 28;
            // 
            // lblPlayerName
            // 
            this.lblPlayerName.AutoSize = true;
            this.lblPlayerName.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPlayerName.Location = new System.Drawing.Point(40, 198);
            this.lblPlayerName.Name = "lblPlayerName";
            this.lblPlayerName.Size = new System.Drawing.Size(121, 20);
            this.lblPlayerName.TabIndex = 26;
            this.lblPlayerName.Text = "<Spelersnaam>";
            // 
            // imgPlayer
            // 
            this.imgPlayer.Location = new System.Drawing.Point(41, 223);
            this.imgPlayer.Name = "imgPlayer";
            this.imgPlayer.Size = new System.Drawing.Size(120, 107);
            this.imgPlayer.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.imgPlayer.TabIndex = 25;
            this.imgPlayer.TabStop = false;
            // 
            // frmTrainingHistory
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.dtpDate);
            this.Controls.Add(this.lblPoints3);
            this.Controls.Add(this.lblPoints2);
            this.Controls.Add(this.lblPoints1);
            this.Controls.Add(this.lblEffort);
            this.Controls.Add(this.lblShape);
            this.Controls.Add(this.lblAttitude);
            this.Controls.Add(this.txtEffort);
            this.Controls.Add(this.txtAttitude);
            this.Controls.Add(this.txtShape);
            this.Controls.Add(this.lblComment);
            this.Controls.Add(this.txtComment);
            this.Controls.Add(this.lblPlayerName);
            this.Controls.Add(this.imgPlayer);
            this.Controls.Add(this.btnEdit);
            this.Controls.Add(this.btnDelete);
            this.Controls.Add(this.btnAdd);
            this.Controls.Add(this.imgTeam);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.lblUsername);
            this.Controls.Add(this.lblTeamName);
            this.Controls.Add(this.imgCoach);
            this.Controls.Add(this.btnNext);
            this.Controls.Add(this.btnPrevious);
            this.Name = "frmTrainingHistory";
            this.Text = "frmTrainingHistory";
            ((System.ComponentModel.ISupportInitialize)(this.imgCoach)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgTeam)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgPlayer)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnPrevious;
        private System.Windows.Forms.Button btnNext;
        private System.Windows.Forms.PictureBox imgCoach;
        private System.Windows.Forms.Label lblTeamName;
        private System.Windows.Forms.Label lblUsername;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.PictureBox imgTeam;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Button btnEdit;
        private System.Windows.Forms.DateTimePicker dtpDate;
        private System.Windows.Forms.Label lblPoints3;
        private System.Windows.Forms.Label lblPoints2;
        private System.Windows.Forms.Label lblPoints1;
        private System.Windows.Forms.Label lblEffort;
        private System.Windows.Forms.Label lblShape;
        private System.Windows.Forms.Label lblAttitude;
        private System.Windows.Forms.TextBox txtEffort;
        private System.Windows.Forms.TextBox txtAttitude;
        private System.Windows.Forms.TextBox txtShape;
        private System.Windows.Forms.Label lblComment;
        private System.Windows.Forms.TextBox txtComment;
        private System.Windows.Forms.Label lblPlayerName;
        private System.Windows.Forms.PictureBox imgPlayer;
    }
}