﻿namespace FootballCoachingPlatform
{
    partial class frmFootballTeam
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.imgCreateTeam = new System.Windows.Forms.PictureBox();
            this.lblTeamName = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.imgCreateTeam)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(104, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(428, 55);
            this.label1.TabIndex = 3;
            this.label1.Text = "Mijn voetbalploeg:";
            // 
            // imgCreateTeam
            // 
            this.imgCreateTeam.Image = global::FootballCoachingPlatform.Properties.Resources.NotAdded;
            this.imgCreateTeam.Location = new System.Drawing.Point(139, 226);
            this.imgCreateTeam.Name = "imgCreateTeam";
            this.imgCreateTeam.Size = new System.Drawing.Size(314, 261);
            this.imgCreateTeam.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.imgCreateTeam.TabIndex = 1;
            this.imgCreateTeam.TabStop = false;
            this.imgCreateTeam.Click += new System.EventHandler(this.imgCreateTeam_Click);
            // 
            // lblTeamName
            // 
            this.lblTeamName.AutoSize = true;
            this.lblTeamName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblTeamName.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTeamName.Location = new System.Drawing.Point(151, 156);
            this.lblTeamName.Name = "lblTeamName";
            this.lblTeamName.Size = new System.Drawing.Size(293, 35);
            this.lblTeamName.TabIndex = 4;
            this.lblTeamName.Text = "Nog niet aangemaakt";
            // 
            // frmFootballTeam
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(623, 544);
            this.Controls.Add(this.lblTeamName);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.imgCreateTeam);
            this.Name = "frmFootballTeam";
            this.Text = "frmFootballTeams";
            ((System.ComponentModel.ISupportInitialize)(this.imgCreateTeam)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.PictureBox imgCreateTeam;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblTeamName;
    }
}