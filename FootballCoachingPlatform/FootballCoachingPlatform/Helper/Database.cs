﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;

namespace FootballCoachingPlatform.Helper
{
    public class Database
    {

        private static MySqlConnection connection;

        public static MySqlConnection getConnection()
        {
            if (Database.connection == null)
            {
                MySqlConnectionStringBuilder connectionStringBuilder = new MySqlConnectionStringBuilder();

                connectionStringBuilder.Server = "localhost";
                connectionStringBuilder.Port = 3307;
                connectionStringBuilder.UserID = "root";
                connectionStringBuilder.Password = "usbw";
                connectionStringBuilder.Database = "FootballCoachingPlatform";

                connection = new MySqlConnection(connectionStringBuilder.ToString());
            }

            return Database.connection;
        }
        
    }
}
