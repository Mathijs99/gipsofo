﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using FootballCoachingPlatform.Model;
using System.Windows.Forms;

namespace FootballCoachingPlatform.Helper
{
    public class Initializer
    {
        private static MySqlConnection connection = Database.getConnection();

        public static void execute()
        {
            try
            {

                String query = "SELECT training.datum, training.houding, training.inzet, training.vorm, training.opmerking, speler.spelerid " +
                    "FROM footballcoachingplatform.training " +
                    "INNER JOIN footballcoachingplatform.speler " +
                    "ON speler.spelerid = training.spelerid " +
                    "WHERE speler.coach = @coach";

                connection.Open();

                MySqlCommand cmd = new MySqlCommand(query, connection);

                cmd.Parameters.AddWithValue("@coach", Coach.username);

                MySqlDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    Training.getTrainingMap().Add(new TrainingKey(Convert.ToInt32(reader["spelerid"]), reader["datum"].ToString()), new Training(Convert.ToDouble(reader["houding"]), Convert.ToDouble(reader["inzet"]), Convert.ToDouble(reader["vorm"]), reader["opmerking"].ToString()));

                    int playerid = Convert.ToInt32(reader["spelerid"]);
                    double attitude = Convert.ToDouble(reader["houding"]);
                    double effort = Convert.ToDouble(reader["inzet"]);
                    double shape = Convert.ToDouble(reader["vorm"]);

                    if (!TrainingTotal.getTrainingTotalMap().Keys.Contains(playerid))
                    {
                        TrainingTotal.getTrainingTotalMap().Add(Convert.ToInt32(reader["spelerid"]), new TrainingTotal(1, attitude, effort, shape));
                    }
                    else
                    {
                        TrainingTotal.getTrainingTotalMap()[playerid].attitude += attitude;
                        TrainingTotal.getTrainingTotalMap()[playerid].effort += effort;
                        TrainingTotal.getTrainingTotalMap()[playerid].shape += shape;
                        TrainingTotal.getTrainingTotalMap()[playerid].total += 1;
                    }
                }
            

                query = "SELECT naam, afbeelding FROM footballcoachingplatform.ploeg WHERE coach = @coach";

                cmd = new MySqlCommand(query, connection);

                cmd.Parameters.AddWithValue("@coach", Coach.username);

                reader.Close();
                reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    Team.name = reader["naam"].ToString();
                    Team.imagePath = reader["afbeelding"].ToString();
                }

                query = "SELECT spelerid, voornaam, naam, leeftijd, favorietePositie, nevenPositie, afbeelding FROM footballcoachingplatform.speler WHERE coach = @coach";

                cmd = new MySqlCommand(query, connection);

                cmd.Parameters.AddWithValue("@coach", Coach.username);

                reader.Close();
                reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    Player.getAll().Add(new Player(Convert.ToInt32(reader["spelerid"]), reader["voornaam"].ToString(), reader["naam"].ToString(), Convert.ToInt32(reader["leeftijd"]), reader["favorietePositie"].ToString(), reader["nevenPositie"].ToString(), reader["afbeelding"].ToString()));
                }

                connection.Close();

            } catch (Exception exception)
            {
                connection.Close();
                MessageBox.Show("Foutmelding: " + exception);
            }
        }
    }
}
