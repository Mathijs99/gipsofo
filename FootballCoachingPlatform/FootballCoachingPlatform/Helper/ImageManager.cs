﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;

namespace FootballCoachingPlatform.Helper
{
    public class ImageManager
    {
        public static String handle(PictureBox pictureBox)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.InitialDirectory = @"C:\";
            openFileDialog.Multiselect = false;
            openFileDialog.Title = "Kies jouw foto";
            openFileDialog.RestoreDirectory = true;
            openFileDialog.Filter =
"Images (*.BMP;*.JPG;*.GIF,*.PNG,*.TIFF)|*.BMP;*.JPG;*.GIF;*.PNG;*.TIFF|" +
"All files (*.*)|*.*";

            DialogResult result = openFileDialog.ShowDialog();

            if (result == DialogResult.OK)
            {
                try
                {
                    pictureBox.Image = Image.FromFile(openFileDialog.FileName);
                    return openFileDialog.FileName;

                } catch (Exception ex)
                {
                    MessageBox.Show("Foutmeldingin: " + ex);
                }
                
            }

            return String.Empty;
        }
    }
}
