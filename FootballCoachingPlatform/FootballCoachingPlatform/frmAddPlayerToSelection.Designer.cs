﻿namespace FootballCoachingPlatform
{
    partial class frmAddPlayerToSelection
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.imgTeam = new System.Windows.Forms.PictureBox();
            this.btnAddPlayerToSelection = new System.Windows.Forms.Button();
            this.lblType = new System.Windows.Forms.Label();
            this.imgCoach = new System.Windows.Forms.PictureBox();
            this.lblUsername = new System.Windows.Forms.Label();
            this.lblTeamName = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lblPlayerName = new System.Windows.Forms.Label();
            this.imgPlayer = new System.Windows.Forms.PictureBox();
            this.btnPrevious = new System.Windows.Forms.Button();
            this.btnNext = new System.Windows.Forms.Button();
            this.txtFavoritePosition = new System.Windows.Forms.TextBox();
            this.txtAlternativePosition = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtAverageEffort = new System.Windows.Forms.TextBox();
            this.txtAverageAttitude = new System.Windows.Forms.TextBox();
            this.txtAverageShape = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.imgTeam)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgCoach)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgPlayer)).BeginInit();
            this.SuspendLayout();
            // 
            // imgTeam
            // 
            this.imgTeam.Location = new System.Drawing.Point(668, 21);
            this.imgTeam.Name = "imgTeam";
            this.imgTeam.Size = new System.Drawing.Size(99, 80);
            this.imgTeam.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.imgTeam.TabIndex = 40;
            this.imgTeam.TabStop = false;
            // 
            // btnAddPlayerToSelection
            // 
            this.btnAddPlayerToSelection.BackColor = System.Drawing.Color.MidnightBlue;
            this.btnAddPlayerToSelection.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddPlayerToSelection.ForeColor = System.Drawing.Color.PeachPuff;
            this.btnAddPlayerToSelection.Location = new System.Drawing.Point(296, 371);
            this.btnAddPlayerToSelection.Name = "btnAddPlayerToSelection";
            this.btnAddPlayerToSelection.Size = new System.Drawing.Size(219, 58);
            this.btnAddPlayerToSelection.TabIndex = 35;
            this.btnAddPlayerToSelection.Text = "Voeg toe aan selectie";
            this.btnAddPlayerToSelection.UseVisualStyleBackColor = false;
            this.btnAddPlayerToSelection.Click += new System.EventHandler(this.btnAddPlayerToSelection_Click);
            // 
            // lblType
            // 
            this.lblType.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblType.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblType.Location = new System.Drawing.Point(420, 21);
            this.lblType.Name = "lblType";
            this.lblType.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.lblType.Size = new System.Drawing.Size(242, 80);
            this.lblType.TabIndex = 34;
            this.lblType.Text = "Mijn team";
            // 
            // imgCoach
            // 
            this.imgCoach.Location = new System.Drawing.Point(34, 21);
            this.imgCoach.Name = "imgCoach";
            this.imgCoach.Size = new System.Drawing.Size(99, 80);
            this.imgCoach.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.imgCoach.TabIndex = 33;
            this.imgCoach.TabStop = false;
            // 
            // lblUsername
            // 
            this.lblUsername.AutoSize = true;
            this.lblUsername.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblUsername.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUsername.Location = new System.Drawing.Point(139, 74);
            this.lblUsername.Name = "lblUsername";
            this.lblUsername.Size = new System.Drawing.Size(196, 27);
            this.lblUsername.TabIndex = 32;
            this.lblUsername.Text = "<Gebruikersnaam>";
            // 
            // lblTeamName
            // 
            this.lblTeamName.AutoSize = true;
            this.lblTeamName.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblTeamName.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTeamName.Location = new System.Drawing.Point(139, 21);
            this.lblTeamName.Name = "lblTeamName";
            this.lblTeamName.Size = new System.Drawing.Size(135, 27);
            this.lblTeamName.TabIndex = 31;
            this.lblTeamName.Text = "<Clubnaam>";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(206, 307);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(91, 16);
            this.label3.TabIndex = 30;
            this.label3.Text = "Nevenpositie:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(206, 268);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(111, 16);
            this.label2.TabIndex = 29;
            this.label2.Text = "Favoriete positie:";
            // 
            // lblPlayerName
            // 
            this.lblPlayerName.AutoSize = true;
            this.lblPlayerName.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPlayerName.Location = new System.Drawing.Point(204, 218);
            this.lblPlayerName.Name = "lblPlayerName";
            this.lblPlayerName.Size = new System.Drawing.Size(0, 25);
            this.lblPlayerName.TabIndex = 28;
            // 
            // imgPlayer
            // 
            this.imgPlayer.Location = new System.Drawing.Point(60, 218);
            this.imgPlayer.Name = "imgPlayer";
            this.imgPlayer.Size = new System.Drawing.Size(123, 105);
            this.imgPlayer.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.imgPlayer.TabIndex = 27;
            this.imgPlayer.TabStop = false;
            // 
            // btnPrevious
            // 
            this.btnPrevious.Location = new System.Drawing.Point(34, 406);
            this.btnPrevious.Name = "btnPrevious";
            this.btnPrevious.Size = new System.Drawing.Size(75, 23);
            this.btnPrevious.TabIndex = 26;
            this.btnPrevious.Text = "Vorige";
            this.btnPrevious.UseVisualStyleBackColor = true;
            this.btnPrevious.Click += new System.EventHandler(this.btnPrevious_Click);
            // 
            // btnNext
            // 
            this.btnNext.Location = new System.Drawing.Point(692, 406);
            this.btnNext.Name = "btnNext";
            this.btnNext.Size = new System.Drawing.Size(75, 23);
            this.btnNext.TabIndex = 25;
            this.btnNext.Text = "Volgende";
            this.btnNext.UseVisualStyleBackColor = true;
            this.btnNext.Click += new System.EventHandler(this.btnNext_Click);
            // 
            // txtFavoritePosition
            // 
            this.txtFavoritePosition.Location = new System.Drawing.Point(345, 268);
            this.txtFavoritePosition.Name = "txtFavoritePosition";
            this.txtFavoritePosition.ReadOnly = true;
            this.txtFavoritePosition.Size = new System.Drawing.Size(100, 20);
            this.txtFavoritePosition.TabIndex = 41;
            // 
            // txtAlternativePosition
            // 
            this.txtAlternativePosition.Location = new System.Drawing.Point(345, 307);
            this.txtAlternativePosition.Name = "txtAlternativePosition";
            this.txtAlternativePosition.ReadOnly = true;
            this.txtAlternativePosition.Size = new System.Drawing.Size(100, 20);
            this.txtAlternativePosition.TabIndex = 42;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(498, 307);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(119, 16);
            this.label1.TabIndex = 43;
            this.label1.Text = "Gemiddelde vorm:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(498, 268);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(116, 16);
            this.label4.TabIndex = 44;
            this.label4.Text = "Gemiddelde inzet:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(498, 225);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(137, 16);
            this.label5.TabIndex = 45;
            this.label5.Text = "Gemiddelde houding:";
            // 
            // txtAverageEffort
            // 
            this.txtAverageEffort.Location = new System.Drawing.Point(641, 264);
            this.txtAverageEffort.Name = "txtAverageEffort";
            this.txtAverageEffort.ReadOnly = true;
            this.txtAverageEffort.Size = new System.Drawing.Size(100, 20);
            this.txtAverageEffort.TabIndex = 46;
            // 
            // txtAverageAttitude
            // 
            this.txtAverageAttitude.Location = new System.Drawing.Point(641, 224);
            this.txtAverageAttitude.Name = "txtAverageAttitude";
            this.txtAverageAttitude.ReadOnly = true;
            this.txtAverageAttitude.Size = new System.Drawing.Size(100, 20);
            this.txtAverageAttitude.TabIndex = 47;
            // 
            // txtAverageShape
            // 
            this.txtAverageShape.Location = new System.Drawing.Point(641, 306);
            this.txtAverageShape.Name = "txtAverageShape";
            this.txtAverageShape.ReadOnly = true;
            this.txtAverageShape.Size = new System.Drawing.Size(100, 20);
            this.txtAverageShape.TabIndex = 48;
            // 
            // frmAddPlayerToSelection
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.txtAverageShape);
            this.Controls.Add(this.txtAverageAttitude);
            this.Controls.Add(this.txtAverageEffort);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtAlternativePosition);
            this.Controls.Add(this.txtFavoritePosition);
            this.Controls.Add(this.imgTeam);
            this.Controls.Add(this.btnAddPlayerToSelection);
            this.Controls.Add(this.lblType);
            this.Controls.Add(this.imgCoach);
            this.Controls.Add(this.lblUsername);
            this.Controls.Add(this.lblTeamName);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.lblPlayerName);
            this.Controls.Add(this.imgPlayer);
            this.Controls.Add(this.btnPrevious);
            this.Controls.Add(this.btnNext);
            this.Name = "frmAddPlayerToSelection";
            this.Text = "frmAddPlayerToSelection";
            ((System.ComponentModel.ISupportInitialize)(this.imgTeam)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgCoach)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgPlayer)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.PictureBox imgTeam;
        private System.Windows.Forms.Button btnAddPlayerToSelection;
        private System.Windows.Forms.Label lblType;
        private System.Windows.Forms.PictureBox imgCoach;
        private System.Windows.Forms.Label lblUsername;
        private System.Windows.Forms.Label lblTeamName;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblPlayerName;
        private System.Windows.Forms.PictureBox imgPlayer;
        private System.Windows.Forms.Button btnPrevious;
        private System.Windows.Forms.Button btnNext;
        private System.Windows.Forms.TextBox txtFavoritePosition;
        private System.Windows.Forms.TextBox txtAlternativePosition;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtAverageEffort;
        private System.Windows.Forms.TextBox txtAverageAttitude;
        private System.Windows.Forms.TextBox txtAverageShape;
    }
}