﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FootballCoachingPlatform
{
    class Player
    {
        private String name;
        private int age;
        private String favoritePosition;
        private String alternativePosition;

        public String getName()
        {
            return name;
        }

        public void setName(String name)
        {
            this.name = name;
        }

        public int getAge()
        {
            return age;
        }

        public void setAge(int age)
        {
            this.age = age;
        }

        public String getFavoritePosition()
        {
            return favoritePosition;
        }

        public void setFavoritePosition(String favoritePosition)
        {
            this.favoritePosition = favoritePosition; ;
        }

        public String getAlternativePosition()
        {
            return alternativePosition;
        }

        public void setAlternativePosition(String alternativePosition)
        {
            this.alternativePosition = alternativePosition;
        }




    }
}
