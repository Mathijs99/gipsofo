﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using FootballCoachingPlatform.DA;

namespace FootballCoachingPlatform
{
    public partial class frmNoAccount : Form
    {
        private bool firstCheck = true;

        public frmNoAccount()
        {
            InitializeComponent();
        }

        private void frmNoAccount_FormClosing(object sender, FormClosingEventArgs e)
        {
            new Main().Show();
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (firstCheck)
            {
                System.Diagnostics.Process.Start("https://www.hln.be");
                chkTaC.Checked = false;
                firstCheck = false;
            }
            
        }

        private void btnMakeAccount_Click(object sender, EventArgs e)
        {
            if (!chkTaC.Checked)
            {
                MessageBox.Show("Algemene voorwaarden niet goed gegekeurd.", "Algemene voorwaarden", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            String username = txtUsername.Text;
            String firstname = txtFirstname.Text;
            String lastname = txtLastname.Text;
            String mail = txtMail.Text;
            String password = txtPassword.Text;
            String repeatedPassword = txtRepeatedPassword.Text;

            checkIfEmpty(username, firstname, lastname, mail, password, repeatedPassword);

            //query om te kijken of de username al in de database zit
            if (CoachingPlatformDA.checkIfUsernameExists(username))
            {
                MessageBox.Show("De gebruikersnaam \"" + username + "\" bestaat al. U moet een andere gebruikersnaam kiezen", "Gebruikersnaam", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtUsername.Clear();
                txtUsername.Focus();
                return;
            }

            if (password != repeatedPassword)
            {
                MessageBox.Show("Wachtwoorden komen niet overeen", "Wachtwoorden", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtPassword.Clear();
                txtRepeatedPassword.Clear();
                txtPassword.Focus();
                return;
            }

            CoachingPlatformDA.addUser(username, firstname, lastname, mail, password);
        }

        private void checkIfEmpty(String username, String firstname, String lastname, String mail, String password, String repeatedPassword)
        {
            if (username == String.Empty)
            {
                MessageBox.Show("Gebruikersnaam niet ingvuld!", "Gebruikersnaam", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtUsername.Focus();
                return;
            }

            if (firstname == String.Empty)
            {
                MessageBox.Show("Voornaam niet ingvuld!", "Voornaam", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtFirstname.Focus();
                return;
            }

            if (lastname == String.Empty)
            {
                MessageBox.Show("Achternaam niet ingvuld!", "Achternaam", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtLastname.Focus();
                return;
            }

            if (mail == String.Empty)
            {
                MessageBox.Show("E-mailadres niet ingvuld!", "E-mailadres", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtMail.Focus();
                return;
            }

            if (password == String.Empty)
            {
                MessageBox.Show("Wachtwoord niet ingvuld!", "Wachtwoord", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtUsername.Focus();
                return;
            }

            if (repeatedPassword == String.Empty)
            {
                MessageBox.Show("Herhaal wachtwoord niet ingvuld!", "Herhaal wachtwoord", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtRepeatedPassword.Focus();
                return;
            }
        }
    }
}
