﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Collections;
using FootballCoachingPlatform.Helper;

namespace FootballCoachingPlatform
{
    public partial class Main : Form
    {
        private ArrayList myList;

        public Main()
        {
            InitializeComponent();
            loadImages();
            myTimer.Start();
        }

        private void myTimer_Tick(object sender, EventArgs e)
        {
            imgBanner.Image = (Image) myList[new Random().Next(0, 10)];
        }

        private void loadImages()
        {
            myList = new ArrayList();

            myList.Add(Properties.Resources.footballCoaching1);
            myList.Add(Properties.Resources.footballCoaching2);
            myList.Add(Properties.Resources.footballCoaching3);
            myList.Add(Properties.Resources.footballCoaching4);
            myList.Add(Properties.Resources.footballCoaching5);
            myList.Add(Properties.Resources.footballCoaching6);
            myList.Add(Properties.Resources.footballCoaching7);
            myList.Add(Properties.Resources.footballCoaching8);
            myList.Add(Properties.Resources.footballCoaching9);
            myList.Add(Properties.Resources.footballCoaching10);
        }

        private void lblForgotPassword_Click(object sender, EventArgs e)
        {
            new frmForgotPassword().Show();
            this.Hide();
        }

        private void lblNoAccount_Click(object sender, EventArgs e)
        {
            new frmNoAccount().Show();
            this.Hide();
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            String username = txtUsername.Text;
            String password = txtPassword.Text;

            /*if (!Database.checkIfPasswordCorresponds(username, password))
            {
                MessageBox.Show("Het ingegeven wachwoord is onjuist. Herbekijk gebruikersnaam en wachtwoord.", "Onjuist wachtwoord", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtPassword.Clear();
                txtPassword.Focus();

                return;
            }*/

            new frmAddPlayer().Show();
            new frmFootballTeams().Show();
            this.Hide();
        }
    }
}