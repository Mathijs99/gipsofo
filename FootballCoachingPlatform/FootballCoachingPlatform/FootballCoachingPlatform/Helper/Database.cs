﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;

namespace FootballCoachingPlatform.Helper
{
    public class Database
    {

        public static MySqlConnection makeConnection()
        {
            MySqlConnectionStringBuilder connectionStringBuilder = new MySqlConnectionStringBuilder();

            connectionStringBuilder.Server = "localhost";
            connectionStringBuilder.Port = 3307;
            connectionStringBuilder.UserID = "root";
            connectionStringBuilder.Password = "usbw";
            connectionStringBuilder.Database = "FootballCoachingPlatform";

            MySqlConnection connection = new MySqlConnection(connectionStringBuilder.ToString());

            connection.Open();

            return connection;
        }
        
    }
}
