﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FootballCoachingPlatform
{
    public partial class frmAddPlayer : Form
    {
        public frmAddPlayer()
        {
            InitializeComponent();

            cboFavoritePosition.Items.Add("Aanvaller");
            cboFavoritePosition.Items.Add("Middenvelder");
            cboFavoritePosition.Items.Add("Verdediger");
            cboFavoritePosition.Items.Add("Keeper");
            cboAlternativePosition.Items.Add("Aanvaller");
            cboAlternativePosition.Items.Add("Middenvelder");
            cboAlternativePosition.Items.Add("Verdediger");
            cboAlternativePosition.Items.Add("Keeper");
        }

        private void btnUpload_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start("explorer.exe");
        }

        private void btnAddPlayer_Click(object sender, EventArgs e)
        {
            String firstname = txtFirstname.Text;
            String lastname = txtLastname.Text;
            DateTime dateOfBirth = dtpDateOfBirth.Value;
            String favoritePosition = cboFavoritePosition.SelectedText;
            String alternativePosition = cboAlternativePosition.SelectedText;

            checkIfEmpty(firstname, lastname, favoritePosition, alternativePosition);

        }

        private void checkIfEmpty(String firstname, String lastname, String favoritePosition, String alternativePosition)
        {
            if (firstname == String.Empty)
            {
                MessageBox.Show("Voornaam niet ingvuld!", "Voornaam", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtFirstname.Focus();
                return;
            }

            if (lastname == String.Empty)
            {
                MessageBox.Show("Achternaam niet ingvuld!", "Achternaam", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtLastname.Focus();
                return;
            }

            if (favoritePosition == String.Empty)
            {
                MessageBox.Show("Favoritie positie niet gekozen!", "Favoriete positie", MessageBoxButtons.OK, MessageBoxIcon.Information);
                cboFavoritePosition.Focus();
                return;
            }

            if (alternativePosition == String.Empty)
            {
                MessageBox.Show("Neven positie niet ingvuld!", "Voornaam", MessageBoxButtons.OK, MessageBoxIcon.Information);
                cboAlternativePosition.Focus();
                return;
            }


        }
    }
}
