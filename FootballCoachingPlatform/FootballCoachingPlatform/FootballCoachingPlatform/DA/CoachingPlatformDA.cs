﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using FootballCoachingPlatform.Helper;

namespace FootballCoachingPlatform.DA
{
    public class CoachingPlatformDA
    {
        public static void addUser(String username, String firstname, String lastname, String mail, String password)
        {
            String query = "INSERT INTO FootballCoachingPlatform.Users (Username, Firstname, Lastname, Mail, Password) VALUES (@username, @firstname, @lastname, @mail, @password)";

            MySqlConnection connection = Database.makeConnection();

            //commando maken
            MySqlCommand cmd = new MySqlCommand(query, connection);

            //parameters toevoegen
            cmd.CommandText = query;
            cmd.Parameters.AddWithValue("@username", username);
            cmd.Parameters.AddWithValue("@firstname", firstname);
            cmd.Parameters.AddWithValue("@lastname", lastname);
            cmd.Parameters.AddWithValue("@mail", mail);
            cmd.Parameters.AddWithValue("@password", password);

            cmd.ExecuteScalar();

            connection.Close();
        }

        public static bool checkIfUsernameExists(String username)
        {

            String query = "SELECT Username FROM FootballCoachingPlatform.Users";

            MySqlConnection connection = Database.makeConnection();

            MySqlCommand cmd = new MySqlCommand(query, connection);

            //cmd.CommandText = query;

            MySqlDataReader reader = cmd.ExecuteReader();

            while (reader.Read())
            {
                if (username == reader["Username"].ToString())
                {
                    connection.Close();
                    return true;
                }

            }

            connection.Close();
            return false;
        }

        public static bool checkIfPasswordCorresponds(String username, String password)
        {
            String query = "SELECT Username, Password FROM FootballCoachingPlatform.Users";

            MySqlConnection connection = Database.makeConnection();

            MySqlCommand cmd = new MySqlCommand(query, connection);

            MySqlDataReader reader = cmd.ExecuteReader();

            while (reader.Read())
            {
                if (username == reader["Username"].ToString() && password == reader["Password"].ToString())
                {
                    connection.Close();
                    return true;
                }

            }

            connection.Close();
            return false;
        }
    }
}
