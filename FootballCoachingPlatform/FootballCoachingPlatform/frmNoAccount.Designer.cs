﻿namespace FootballCoachingPlatform
{
    partial class frmNoAccount
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.txtUsername = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.txtFirstname = new System.Windows.Forms.TextBox();
            this.txtPassword = new System.Windows.Forms.TextBox();
            this.txtRepeatedPassword = new System.Windows.Forms.TextBox();
            this.txtMail = new System.Windows.Forms.TextBox();
            this.txtLastname = new System.Windows.Forms.TextBox();
            this.btnMakeAccount = new System.Windows.Forms.Button();
            this.chkTaC = new System.Windows.Forms.CheckBox();
            this.label2 = new System.Windows.Forms.Label();
            this.btnUploadImage = new System.Windows.Forms.Button();
            this.imgImage = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.imgImage)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(88, 108);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(55, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Voornaam";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(79, 152);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(64, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Achternaam";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(82, 197);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(61, 13);
            this.label6.TabIndex = 5;
            this.label6.Text = "E-mailadres";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(59, 62);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(84, 13);
            this.label7.TabIndex = 6;
            this.label7.Text = "Gebruikersnaam";
            // 
            // txtUsername
            // 
            this.txtUsername.Location = new System.Drawing.Point(172, 59);
            this.txtUsername.Name = "txtUsername";
            this.txtUsername.Size = new System.Drawing.Size(177, 20);
            this.txtUsername.TabIndex = 7;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(38, 288);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(105, 13);
            this.label8.TabIndex = 8;
            this.label8.Text = "Herhaal wachtwoord";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(75, 244);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(68, 13);
            this.label9.TabIndex = 9;
            this.label9.Text = "Wachtwoord";
            // 
            // txtFirstname
            // 
            this.txtFirstname.Location = new System.Drawing.Point(172, 105);
            this.txtFirstname.Name = "txtFirstname";
            this.txtFirstname.Size = new System.Drawing.Size(177, 20);
            this.txtFirstname.TabIndex = 18;
            // 
            // txtPassword
            // 
            this.txtPassword.Location = new System.Drawing.Point(172, 241);
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.PasswordChar = '•';
            this.txtPassword.Size = new System.Drawing.Size(177, 20);
            this.txtPassword.TabIndex = 19;
            // 
            // txtRepeatedPassword
            // 
            this.txtRepeatedPassword.Location = new System.Drawing.Point(172, 285);
            this.txtRepeatedPassword.Name = "txtRepeatedPassword";
            this.txtRepeatedPassword.PasswordChar = '•';
            this.txtRepeatedPassword.Size = new System.Drawing.Size(177, 20);
            this.txtRepeatedPassword.TabIndex = 20;
            // 
            // txtMail
            // 
            this.txtMail.Location = new System.Drawing.Point(172, 194);
            this.txtMail.Name = "txtMail";
            this.txtMail.Size = new System.Drawing.Size(177, 20);
            this.txtMail.TabIndex = 23;
            // 
            // txtLastname
            // 
            this.txtLastname.Location = new System.Drawing.Point(172, 149);
            this.txtLastname.Name = "txtLastname";
            this.txtLastname.Size = new System.Drawing.Size(177, 20);
            this.txtLastname.TabIndex = 24;
            // 
            // btnMakeAccount
            // 
            this.btnMakeAccount.BackColor = System.Drawing.Color.DarkSeaGreen;
            this.btnMakeAccount.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMakeAccount.Location = new System.Drawing.Point(172, 475);
            this.btnMakeAccount.Name = "btnMakeAccount";
            this.btnMakeAccount.Size = new System.Drawing.Size(177, 47);
            this.btnMakeAccount.TabIndex = 25;
            this.btnMakeAccount.Text = "Maak account aan";
            this.btnMakeAccount.UseVisualStyleBackColor = false;
            this.btnMakeAccount.Click += new System.EventHandler(this.btnMakeAccount_Click);
            // 
            // chkTaC
            // 
            this.chkTaC.AutoSize = true;
            this.chkTaC.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkTaC.ForeColor = System.Drawing.Color.CornflowerBlue;
            this.chkTaC.Location = new System.Drawing.Point(43, 442);
            this.chkTaC.Name = "chkTaC";
            this.chkTaC.Size = new System.Drawing.Size(306, 17);
            this.chkTaC.TabIndex = 26;
            this.chkTaC.Text = "Ik heb de algemene voorwaarden gelezen en goekgekeurd";
            this.chkTaC.UseVisualStyleBackColor = true;
            this.chkTaC.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(21, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(368, 20);
            this.label2.TabIndex = 27;
            this.label2.Text = "Nog geen account? Maak hier een account aan!";
            // 
            // btnUploadImage
            // 
            this.btnUploadImage.BackColor = System.Drawing.Color.DarkRed;
            this.btnUploadImage.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUploadImage.ForeColor = System.Drawing.Color.White;
            this.btnUploadImage.Location = new System.Drawing.Point(172, 329);
            this.btnUploadImage.Name = "btnUploadImage";
            this.btnUploadImage.Size = new System.Drawing.Size(177, 97);
            this.btnUploadImage.TabIndex = 28;
            this.btnUploadImage.Text = "Upload profielfoto";
            this.btnUploadImage.UseVisualStyleBackColor = false;
            this.btnUploadImage.Click += new System.EventHandler(this.btnUploadImage_Click);
            // 
            // imgImage
            // 
            this.imgImage.Image = global::FootballCoachingPlatform.Properties.Resources.onbekend;
            this.imgImage.Location = new System.Drawing.Point(31, 329);
            this.imgImage.Name = "imgImage";
            this.imgImage.Size = new System.Drawing.Size(112, 97);
            this.imgImage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.imgImage.TabIndex = 29;
            this.imgImage.TabStop = false;
            // 
            // frmNoAccount
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(429, 550);
            this.Controls.Add(this.imgImage);
            this.Controls.Add(this.btnUploadImage);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.chkTaC);
            this.Controls.Add(this.btnMakeAccount);
            this.Controls.Add(this.txtLastname);
            this.Controls.Add(this.txtMail);
            this.Controls.Add(this.txtRepeatedPassword);
            this.Controls.Add(this.txtPassword);
            this.Controls.Add(this.txtFirstname);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.txtUsername);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label1);
            this.Name = "frmNoAccount";
            this.Text = "frmNoAccount";
         
            ((System.ComponentModel.ISupportInitialize)(this.imgImage)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtUsername;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtFirstname;
        private System.Windows.Forms.TextBox txtPassword;
        private System.Windows.Forms.TextBox txtRepeatedPassword;
        private System.Windows.Forms.TextBox txtMail;
        private System.Windows.Forms.TextBox txtLastname;
        private System.Windows.Forms.Button btnMakeAccount;
        private System.Windows.Forms.CheckBox chkTaC;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnUploadImage;
        private System.Windows.Forms.PictureBox imgImage;
    }
}