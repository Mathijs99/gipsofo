﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using FootballCoachingPlatform.Model;
using FootballCoachingPlatform.DA;

namespace FootballCoachingPlatform
{
    public partial class frmAddPlayerToSelection : Form
    {
        private List<Player> players;
        private int counter;
        private Player currentPlayer;
        private String position, date;

        private static List<Player> currentAttackers, currentMidfielders, currentDefenders, currentKeeper;

        public static List<Player>[] getSelection()
        {
            return new List<Player>[]
            {
                currentAttackers, currentMidfielders, currentDefenders, currentKeeper
            };
        }

        public frmAddPlayerToSelection(List<Player> players, String position, String date)
        {
            InitializeComponent();
            this.players = players;
            counter = 0;
            this.position = position;
            lblType.Text = position;
            this.date = date;

            currentAttackers = new List<Player>();
            currentMidfielders = new List<Player>();
            currentDefenders = new List<Player>();
            currentKeeper = new List<Player>();

            lblTeamName.Text = Team.name;
            lblUsername.Text = Coach.username;

            if (Player.getAll().Count() == 0)
            {
                MessageBox.Show("Nog geen spelers", "Eerst spelers toevoegen", MessageBoxButtons.OK, MessageBoxIcon.Information);
                new frmAddPlayer().ShowDialog();
                return;
            }

            loadPlayer(players.ElementAt(counter));

            try
            {
                imgCoach.Image = Image.FromFile(Coach.imagePath);
            }
            catch (Exception)
            {
                imgCoach.Image = Properties.Resources.onbekend;
            }

            try
            {
                imgTeam.Image = Image.FromFile(Team.imagePath);
            }
            catch (Exception)
            {
                imgTeam.Image = Properties.Resources.onbekend;
            }
        }

        private void btnAddPlayerToSelection_Click(object sender, EventArgs e)
        {
            if (frmMatchday.selection.Contains(currentPlayer))
            {
                MessageBox.Show("Deze speler zit al in de selectie", "Al in de selecte", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            if (position == "Keeper")
            {
                    currentKeeper.Clear();
                    currentKeeper.Add(currentPlayer);
                frmMatchday.keeperTemp.Add(currentPlayer);
            } else
            {
                if ((frmMatchday.attackerCounter + frmMatchday.midfielderCounter + frmMatchday.defenderCounter) == 10)
                {
                    MessageBox.Show("Er is geen plaats meer in de selectie", "Selectie zit vol", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }

                if (currentAttackers.Count() == 4 || currentMidfielders.Count() == 5 || currentDefenders.Count() == 5 || currentKeeper.Count() == 1)
                {
                    MessageBox.Show("Er kunnen geen spelers meer op deze positie spelen", "Plaatsen op deze positie zitten vol", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }

                if (currentAttackers.Contains(currentPlayer) || currentMidfielders.Contains(currentPlayer) || currentDefenders.Contains(currentPlayer) || currentKeeper.Contains(currentPlayer))
                {
                    MessageBox.Show("Deze speler behoort al tot de selectie", "Zit al in de selectie", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }

                if (position == "Aanvaller")
                {
                    currentAttackers.Add(currentPlayer);
                    frmMatchday.attackerCounter++;
                    frmMatchday.attackersTemp.Add(currentPlayer);
                }
                else if (position == "Middenvelder")
                {
                    currentMidfielders.Add(currentPlayer);
                    frmMatchday.midfielderCounter++;
                    frmMatchday.midfieldersTemp.Add(currentPlayer);
                }
                else if (position == "Verdediger")
                {
                    currentDefenders.Add(currentPlayer);
                    frmMatchday.defenderCounter++;
                    frmMatchday.defendersTemp.Add(currentPlayer);
                }
            }

            players.Remove(currentPlayer);

            /*frmMatchday.attackers.Remove(currentPlayer);
            frmMatchday.midfielders.Remove(currentPlayer);
            frmMatchday.defenders.Remove(currentPlayer);
            frmMatchday.keeper.Remove(currentPlayer);*/

            frmMatchday.selection.Add(currentPlayer);
            MatchData.getMatchDataList().Add(new MatchData(currentPlayer.id, position));

            MessageBox.Show(currentPlayer.firstName + " " + currentPlayer.lastName + " is succesvol toegevoegd tot de selectie", "speler toegevoegd tot de selectie", MessageBoxButtons.OK, MessageBoxIcon.Information);

            frmMatchday.selectionCounter++;

        }

        private void loadPlayer(Player player)
        {
            lblPlayerName.Text = player.firstName + " " + player.lastName;
            txtFavoritePosition.BackColor = Color.White;
            txtAlternativePosition.BackColor = Color.White;

            try
            {
                imgPlayer.Image = Image.FromFile(player.imagePath);
            }
            catch (Exception)
            {
                imgPlayer.Image = Properties.Resources.onbekend;
            }

            txtAlternativePosition.Text = player.alternativePosition;
            txtFavoritePosition.Text = player.favoritePosition;

            if (player.favoritePosition == position)
            {      
                txtFavoritePosition.BackColor = Color.Yellow;
            } else
            {
                txtAlternativePosition.BackColor = Color.Yellow;
            }

            if (TrainingTotal.getTrainingTotalMap().ContainsKey(player.id))
            {
                TrainingTotal trainingTotal = TrainingTotal.getTrainingTotalMap()[player.id];

                txtAverageAttitude.Text = trainingTotal.attitude / trainingTotal.total + "";
                txtAverageEffort.Text = trainingTotal.effort / trainingTotal.total + "";
                txtAverageShape.Text = trainingTotal.shape / trainingTotal.total + "";
            } else
            {
                txtAverageAttitude.Text = "/";
                txtAverageEffort.Text = "/";
                txtAverageShape.Text = "/";
            }  

            currentPlayer = player;
        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            if (players.Count() <= 1)
            {
                return;
            }

            counter++;

            if (counter >= players.Count())
            {
                counter = 0;
            }

            loadPlayer(players.ElementAt(counter));
        }

        private void btnPrevious_Click(object sender, EventArgs e)
        {
            if (players.Count() <= 1)
            {
                return;
            }

            counter--;

            if (counter <= -1)
            {
                counter = players.Count() - 1;
            }

            loadPlayer(players.ElementAt(counter));
        }
    }
}
