﻿namespace FootballCoachingPlatform
{
    partial class frmMyTeam
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnNext = new System.Windows.Forms.Button();
            this.btnPrevious = new System.Windows.Forms.Button();
            this.imgPlayer = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lblTeamName = new System.Windows.Forms.Label();
            this.lblUsername = new System.Windows.Forms.Label();
            this.imgCoach = new System.Windows.Forms.PictureBox();
            this.label6 = new System.Windows.Forms.Label();
            this.btnAddPlayer = new System.Windows.Forms.Button();
            this.btnRemove = new System.Windows.Forms.Button();
            this.btnEdit = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.txtLastname = new System.Windows.Forms.TextBox();
            this.imgTeam = new System.Windows.Forms.PictureBox();
            this.txtFirstname = new System.Windows.Forms.TextBox();
            this.cboFavoritePosition = new System.Windows.Forms.ComboBox();
            this.cboAlternativePosition = new System.Windows.Forms.ComboBox();
            this.txtAge = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.imgPlayer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgCoach)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgTeam)).BeginInit();
            this.SuspendLayout();
            // 
            // btnNext
            // 
            this.btnNext.Location = new System.Drawing.Point(695, 397);
            this.btnNext.Name = "btnNext";
            this.btnNext.Size = new System.Drawing.Size(75, 23);
            this.btnNext.TabIndex = 0;
            this.btnNext.Text = "Volgende";
            this.btnNext.UseVisualStyleBackColor = true;
            this.btnNext.Click += new System.EventHandler(this.btnNext_Click);
            // 
            // btnPrevious
            // 
            this.btnPrevious.Location = new System.Drawing.Point(37, 397);
            this.btnPrevious.Name = "btnPrevious";
            this.btnPrevious.Size = new System.Drawing.Size(75, 23);
            this.btnPrevious.TabIndex = 1;
            this.btnPrevious.Text = "Vorige";
            this.btnPrevious.UseVisualStyleBackColor = true;
            this.btnPrevious.Click += new System.EventHandler(this.btnPrevious_Click);
            // 
            // imgPlayer
            // 
            this.imgPlayer.Location = new System.Drawing.Point(65, 209);
            this.imgPlayer.Name = "imgPlayer";
            this.imgPlayer.Size = new System.Drawing.Size(123, 105);
            this.imgPlayer.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.imgPlayer.TabIndex = 2;
            this.imgPlayer.TabStop = false;
            this.imgPlayer.Click += new System.EventHandler(this.imgPlayer_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(60, 127);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(68, 25);
            this.label1.TabIndex = 3;
            this.label1.Text = "Naam";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(311, 209);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(111, 16);
            this.label2.TabIndex = 4;
            this.label2.Text = "Favoriete positie:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(311, 256);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(91, 16);
            this.label3.TabIndex = 5;
            this.label3.Text = "Nevenpositie:";
            // 
            // lblTeamName
            // 
            this.lblTeamName.AutoSize = true;
            this.lblTeamName.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblTeamName.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTeamName.Location = new System.Drawing.Point(142, 12);
            this.lblTeamName.Name = "lblTeamName";
            this.lblTeamName.Size = new System.Drawing.Size(135, 27);
            this.lblTeamName.TabIndex = 7;
            this.lblTeamName.Text = "<Clubnaam>";
            // 
            // lblUsername
            // 
            this.lblUsername.AutoSize = true;
            this.lblUsername.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblUsername.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUsername.Location = new System.Drawing.Point(142, 65);
            this.lblUsername.Name = "lblUsername";
            this.lblUsername.Size = new System.Drawing.Size(196, 27);
            this.lblUsername.TabIndex = 8;
            this.lblUsername.Text = "<Gebruikersnaam>";
            // 
            // imgCoach
            // 
            this.imgCoach.Location = new System.Drawing.Point(37, 12);
            this.imgCoach.Name = "imgCoach";
            this.imgCoach.Size = new System.Drawing.Size(99, 80);
            this.imgCoach.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.imgCoach.TabIndex = 9;
            this.imgCoach.TabStop = false;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(423, 12);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(242, 57);
            this.label6.TabIndex = 10;
            this.label6.Text = "Mijn team";
            // 
            // btnAddPlayer
            // 
            this.btnAddPlayer.BackColor = System.Drawing.Color.MidnightBlue;
            this.btnAddPlayer.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddPlayer.ForeColor = System.Drawing.Color.PeachPuff;
            this.btnAddPlayer.Location = new System.Drawing.Point(299, 366);
            this.btnAddPlayer.Name = "btnAddPlayer";
            this.btnAddPlayer.Size = new System.Drawing.Size(219, 54);
            this.btnAddPlayer.TabIndex = 11;
            this.btnAddPlayer.Text = "Voeg speler toe";
            this.btnAddPlayer.UseVisualStyleBackColor = false;
            this.btnAddPlayer.Click += new System.EventHandler(this.btnAddPlayer_Click);
            // 
            // btnRemove
            // 
            this.btnRemove.BackColor = System.Drawing.Color.Red;
            this.btnRemove.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRemove.Location = new System.Drawing.Point(636, 269);
            this.btnRemove.Name = "btnRemove";
            this.btnRemove.Size = new System.Drawing.Size(134, 45);
            this.btnRemove.TabIndex = 12;
            this.btnRemove.Text = "Verwijder";
            this.btnRemove.UseVisualStyleBackColor = false;
            this.btnRemove.Click += new System.EventHandler(this.btnRemove_Click);
            // 
            // btnEdit
            // 
            this.btnEdit.BackColor = System.Drawing.Color.YellowGreen;
            this.btnEdit.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEdit.Location = new System.Drawing.Point(636, 209);
            this.btnEdit.Name = "btnEdit";
            this.btnEdit.Size = new System.Drawing.Size(134, 45);
            this.btnEdit.TabIndex = 13;
            this.btnEdit.Text = "Bewerk";
            this.btnEdit.UseVisualStyleBackColor = false;
            this.btnEdit.Click += new System.EventHandler(this.btnEdit_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(311, 301);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(54, 16);
            this.label7.TabIndex = 14;
            this.label7.Text = "Leeftijd:";
            // 
            // txtLastname
            // 
            this.txtLastname.Location = new System.Drawing.Point(65, 183);
            this.txtLastname.Name = "txtLastname";
            this.txtLastname.Size = new System.Drawing.Size(123, 20);
            this.txtLastname.TabIndex = 15;
            // 
            // imgTeam
            // 
            this.imgTeam.Location = new System.Drawing.Point(671, 12);
            this.imgTeam.Name = "imgTeam";
            this.imgTeam.Size = new System.Drawing.Size(99, 80);
            this.imgTeam.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.imgTeam.TabIndex = 20;
            this.imgTeam.TabStop = false;
            // 
            // txtFirstname
            // 
            this.txtFirstname.Location = new System.Drawing.Point(65, 157);
            this.txtFirstname.Name = "txtFirstname";
            this.txtFirstname.Size = new System.Drawing.Size(123, 20);
            this.txtFirstname.TabIndex = 21;
            // 
            // cboFavoritePosition
            // 
            this.cboFavoritePosition.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboFavoritePosition.FormattingEnabled = true;
            this.cboFavoritePosition.Location = new System.Drawing.Point(428, 208);
            this.cboFavoritePosition.Name = "cboFavoritePosition";
            this.cboFavoritePosition.Size = new System.Drawing.Size(118, 21);
            this.cboFavoritePosition.TabIndex = 22;
            // 
            // cboAlternativePosition
            // 
            this.cboAlternativePosition.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboAlternativePosition.FormattingEnabled = true;
            this.cboAlternativePosition.Location = new System.Drawing.Point(428, 255);
            this.cboAlternativePosition.Name = "cboAlternativePosition";
            this.cboAlternativePosition.Size = new System.Drawing.Size(118, 21);
            this.cboAlternativePosition.TabIndex = 23;
            // 
            // txtAge
            // 
            this.txtAge.Location = new System.Drawing.Point(428, 301);
            this.txtAge.Name = "txtAge";
            this.txtAge.Size = new System.Drawing.Size(118, 20);
            this.txtAge.TabIndex = 24;
            // 
            // frmMyTeam
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.txtAge);
            this.Controls.Add(this.cboAlternativePosition);
            this.Controls.Add(this.cboFavoritePosition);
            this.Controls.Add(this.txtFirstname);
            this.Controls.Add(this.imgTeam);
            this.Controls.Add(this.txtLastname);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.btnEdit);
            this.Controls.Add(this.btnRemove);
            this.Controls.Add(this.btnAddPlayer);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.imgCoach);
            this.Controls.Add(this.lblUsername);
            this.Controls.Add(this.lblTeamName);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.imgPlayer);
            this.Controls.Add(this.btnPrevious);
            this.Controls.Add(this.btnNext);
            this.Name = "frmMyTeam";
            this.Text = "MyTeam";
            this.Activated += new System.EventHandler(this.frmMyTeam_Activated);
            ((System.ComponentModel.ISupportInitialize)(this.imgPlayer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgCoach)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgTeam)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnNext;
        private System.Windows.Forms.Button btnPrevious;
        private System.Windows.Forms.PictureBox imgPlayer;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lblTeamName;
        private System.Windows.Forms.Label lblUsername;
        private System.Windows.Forms.PictureBox imgCoach;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button btnAddPlayer;
        private System.Windows.Forms.Button btnRemove;
        private System.Windows.Forms.Button btnEdit;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtLastname;
        private System.Windows.Forms.PictureBox imgTeam;
        private System.Windows.Forms.TextBox txtFirstname;
        private System.Windows.Forms.ComboBox cboFavoritePosition;
        private System.Windows.Forms.ComboBox cboAlternativePosition;
        private System.Windows.Forms.TextBox txtAge;
    }
}