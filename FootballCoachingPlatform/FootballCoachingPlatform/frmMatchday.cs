﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using FootballCoachingPlatform.Model;
using FootballCoachingPlatform.DA;

namespace FootballCoachingPlatform
{
    public partial class frmMatchday : Form
    {

        public static List<Player> attackers { get; set; }
        public static List<Player> midfielders { get; set; }
        public static List<Player> defenders { get; set; }
        public static List<Player> keeper { get; set; }

        public static List<Player> attackersTemp { get; set; }
        public static List<Player> midfieldersTemp { get; set; }
        public static List<Player> defendersTemp { get; set; }
        public static List<Player> keeperTemp { get; set; }

        public static int attackerCounter { get; set; }
        public static int midfielderCounter { get; set; }
        public static int defenderCounter { get; set; }

        public static bool complete { get; set; }

        public static List<Player> selection { get; set; } = new List<Player>();

        public static int selectionCounter { get; set; }

        private void frmMatchday_FormClosing(object sender, FormClosingEventArgs e)
        {
            Array.Clear(frmAddPlayerToSelection.getSelection(), 0, 4);
        }

        private void btnMidfield_Click(object sender, EventArgs e)
        {
            if (midfielders.Count() == 0)
            {
                MessageBox.Show("U heeft geen middenvelders in uw selectie", "Geen middenvelders", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            selectionCounter -= midfielderCounter;
           
            foreach (Player player in midfieldersTemp)
            {
                selection.Remove(player);
            }

            midfielders.AddRange(midfieldersTemp);
            new frmAddPlayerToSelection(midfielders, "Middenvelder", dtpDate.Value.ToShortDateString()).Show();
            midfielderCounter = 0;
            complete = false;

            foreach (Player player in midfieldersTemp)
            {
                MatchData.getMatchDataList().Remove(new MatchData(player.id, "Middenvelder"));
            }

            imgMidfield1.Visible = false;
            lblMidfield1.Visible = false;
            imgMidfield2.Visible = false;
            lblMidfield2.Visible = false;
            imgMidfield3.Visible = false;
            lblMidfield3.Visible = false;
            imgMidfield4.Visible = false;
            lblMidfield4.Visible = false;
            imgMidfield5.Visible = false;
            lblMidfield5.Visible = false;
        }

        private void btnDefence_Click(object sender, EventArgs e)
        {
            if (defenders.Count() == 0)
            {
                MessageBox.Show("U heeft geen verdeigers in uw selectie", "Geen verdedigers", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            foreach (Player player in defendersTemp)
            {
                selection.Remove(player);
            }

            selectionCounter -= defenderCounter;
            defenders.AddRange(defendersTemp);
            new frmAddPlayerToSelection(defenders, "Verdediger", dtpDate.Value.ToShortDateString()).Show();
            defenderCounter = 0;

            foreach (Player player in defendersTemp)
            {
                MatchData.getMatchDataList().Remove(new MatchData(player.id, "Verdediger"));
            }

            imgDefence1.Visible = false;
            lblDefence1.Visible = false;
            imgDefence2.Visible = false;
            lblDefence2.Visible = false;
            imgDefence3.Visible = false;
            lblDefence3.Visible = false;
            imgDefence4.Visible = false;
            lblDefence4.Visible = false;
            imgDefence5.Visible = false;
            lblDefence5.Visible = false;
        }

        private void btnKeeper_Click(object sender, EventArgs e)
        {
            if (keeper.Count() == 0)
            {
                MessageBox.Show("U heeft geen keepers in uw selectie", "Geen keepers", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            foreach (Player player in keeperTemp)
            {
                selection.Remove(player);
            }
            selectionCounter -= 1;
            keeper.AddRange(keeperTemp);
            new frmAddPlayerToSelection(keeper, "Keeper", dtpDate.Value.ToShortDateString()).Show();

            foreach (Player player in keeperTemp)
            {
                MatchData.getMatchDataList().Remove(new MatchData(player.id, "Keeper"));
            }

            imgKeeper.Visible = false;
            lblKeeper.Visible = false;
        }

        private void frmMatchday_Activated(object sender, EventArgs e)
        {
            /*imgAttack1.Visible = false;
            lblAttack1.Visible = false;
            imgAttack2.Visible = false;
            lblAttack2.Visible = false;
            imgAttack3.Visible = false;
            lblAttack3.Visible = false;
            imgAttack4.Visible = false;
            lblAttack4.Visible = false;

            imgMidfield1.Visible = false;
            lblMidfield1.Visible = false;
            imgMidfield2.Visible = false;
            lblMidfield2.Visible = false;
            imgMidfield3.Visible = false;
            lblMidfield3.Visible = false;
            imgMidfield4.Visible = false;
            lblMidfield4.Visible = false;
            imgMidfield5.Visible = false;
            lblMidfield5.Visible = false;

            imgDefence1.Visible = false;
            lblDefence1.Visible = false;
            imgDefence2.Visible = false;
            lblDefence2.Visible = false;
            imgDefence3.Visible = false;
            lblDefence3.Visible = false;
            imgDefence4.Visible = false;
            lblDefence4.Visible = false;
            imgDefence5.Visible = false;
            lblDefence5.Visible = false;

            imgKeeper.Visible = false;
            lblKeeper.Visible = false;*/

            List<Player> currentAttackers = frmAddPlayerToSelection.getSelection()[0];
            if (currentAttackers != null)
            {
                if (currentAttackers.Count() >= 1)
                {
                    try
                    {
                        imgAttack1.Image = Image.FromFile(currentAttackers.ElementAt(0).imagePath);
                    } catch (Exception)
                    {
                        imgAttack1.Image = Properties.Resources.onbekend;
                    }
                    
                    lblAttack1.Text = currentAttackers.ElementAt(0).firstName + " " + currentAttackers.ElementAt(0).lastName;

                    imgAttack1.Visible = true;
                    lblAttack1.Visible = true;

                    if (currentAttackers.Count() >= 2)
                    {
                        try
                        {
                            imgAttack2.Image = Image.FromFile(currentAttackers.ElementAt(1).imagePath);
                        }
                        catch (Exception)
                        {
                            imgAttack2.Image = Properties.Resources.onbekend;
                        }
                        lblAttack2.Text = currentAttackers.ElementAt(1).firstName + " " + currentAttackers.ElementAt(1).lastName;

                        imgAttack2.Visible = true;
                        lblAttack2.Visible = true;

                        if (currentAttackers.Count() >= 3)
                        {
                            try
                            {
                                imgAttack3.Image = Image.FromFile(currentAttackers.ElementAt(2).imagePath);
                            }
                            catch (Exception)
                            {
                                imgAttack3.Image = Properties.Resources.onbekend;
                            }
                            lblAttack3.Text = currentAttackers.ElementAt(2).firstName + " " + currentAttackers.ElementAt(2).lastName;

                            imgAttack3.Visible = true;
                            lblAttack3.Visible = true;

                            if (currentAttackers.Count() >= 4)
                            {
                                try
                                {
                                    imgAttack4.Image = Image.FromFile(currentAttackers.ElementAt(3).imagePath);
                                }
                                catch (Exception)
                                {
                                    imgAttack4.Image = Properties.Resources.onbekend;
                                }
                                lblAttack4.Text = currentAttackers.ElementAt(3).firstName + " " + currentAttackers.ElementAt(3).lastName;

                                imgAttack4.Visible = true;
                                lblAttack4.Visible = true;
                            }
                        }
                    }
                }
            }

            List<Player> currentMidfielders = frmAddPlayerToSelection.getSelection()[1];
            if (currentMidfielders != null)
            {
                if (currentMidfielders.Count() >= 1)
                {
                    try
                    {
                        imgMidfield1.Image = Image.FromFile(currentMidfielders.ElementAt(0).imagePath);
                    }
                    catch (Exception)
                    {
                        imgMidfield1.Image = Properties.Resources.onbekend;
                    }
                    lblMidfield1.Text = currentMidfielders.ElementAt(0).firstName + " " + currentMidfielders.ElementAt(0).lastName;

                    imgMidfield1.Visible = true;
                    lblMidfield1.Visible = true;

                    if (currentMidfielders.Count() >= 2)
                    {
                        try
                        {
                            imgMidfield2.Image = Image.FromFile(currentMidfielders.ElementAt(1).imagePath);
                        }
                        catch (Exception)
                        {
                            imgMidfield2.Image = Properties.Resources.onbekend;
                        }
                        lblMidfield2.Text = currentMidfielders.ElementAt(1).firstName + " " + currentMidfielders.ElementAt(1).lastName;

                        imgMidfield2.Visible = true;
                        lblMidfield2.Visible = true;

                        if (currentMidfielders.Count() >= 3)
                        {
                            try
                            {
                                imgMidfield3.Image = Image.FromFile(currentMidfielders.ElementAt(2).imagePath);
                            }
                            catch (Exception)
                            {
                                imgMidfield3.Image = Properties.Resources.onbekend;
                            }
                            lblMidfield3.Text = currentMidfielders.ElementAt(2).firstName + " " + currentMidfielders.ElementAt(2).lastName;

                            imgMidfield3.Visible = true;
                            lblMidfield3.Visible = true;

                            if (currentMidfielders.Count() >= 4)
                            {
                                try
                                {
                                    imgMidfield4.Image = Image.FromFile(currentMidfielders.ElementAt(3).imagePath);
                                }
                                catch (Exception)
                                {
                                    imgMidfield4.Image = Properties.Resources.onbekend;
                                }
                                lblMidfield4.Text = currentMidfielders.ElementAt(3).firstName + " " + currentMidfielders.ElementAt(3).lastName;

                                imgMidfield4.Visible = true;
                                lblMidfield4.Visible = true;

                                if (currentMidfielders.Count() >= 5)
                                {
                                    try
                                    {
                                        imgMidfield5.Image = Image.FromFile(currentMidfielders.ElementAt(4).imagePath);
                                    }
                                    catch (Exception)
                                    {
                                        imgMidfield5.Image = Properties.Resources.onbekend;
                                    }
                                    lblMidfield5.Text = currentMidfielders.ElementAt(4).firstName + " " + currentMidfielders.ElementAt(4).lastName;

                                    imgMidfield5.Visible = true;
                                    lblMidfield5.Visible = true;
                                }
                            }
                        }
                    }
                }
            }

            List<Player> currentDefenders = frmAddPlayerToSelection.getSelection()[2];

            if (currentDefenders != null)
            {
                if (currentDefenders.Count() >= 1)
                {
                    try
                    {
                        imgDefence1.Image = Image.FromFile(currentDefenders.ElementAt(0).imagePath);
                    }
                    catch (Exception)
                    {
                        imgDefence1.Image = Properties.Resources.onbekend;
                    }
                    lblDefence1.Text = currentDefenders.ElementAt(0).firstName + " " + currentDefenders.ElementAt(0).lastName;

                    imgDefence1.Visible = true;
                    lblDefence1.Visible = true;

                    if (currentDefenders.Count() >= 2)
                    {
                        try
                        {
                            imgDefence2.Image = Image.FromFile(currentDefenders.ElementAt(1).imagePath);
                        }
                        catch (Exception)
                        {
                            imgDefence2.Image = Properties.Resources.onbekend;
                        }
                        lblDefence2.Text = currentDefenders.ElementAt(1).firstName + " " + currentDefenders.ElementAt(1).lastName;

                        imgDefence2.Visible = true;
                        lblDefence2.Visible = true;

                        if (currentDefenders.Count() >= 3)
                        {
                            try
                            {
                                imgDefence3.Image = Image.FromFile(currentDefenders.ElementAt(2).imagePath);
                            }
                            catch (Exception)
                            {
                                imgDefence3.Image = Properties.Resources.onbekend;
                            }
                            lblDefence3.Text = currentDefenders.ElementAt(2).firstName + " " + currentDefenders.ElementAt(2).lastName;

                            imgDefence3.Visible = true;
                            lblDefence3.Visible = true;

                            if (currentDefenders.Count() >= 4)
                            {
                                try
                                {
                                    imgDefence4.Image = Image.FromFile(currentDefenders.ElementAt(3).imagePath);
                                }
                                catch (Exception)
                                {
                                    imgDefence4.Image = Properties.Resources.onbekend;
                                }
                                lblDefence4.Text = currentDefenders.ElementAt(3).firstName + " " + currentDefenders.ElementAt(3).lastName;

                                imgDefence4.Visible = true;
                                lblDefence4.Visible = true;

                                if (currentDefenders.Count() >= 5)
                                {
                                    try
                                    {
                                        imgDefence5.Image = Image.FromFile(currentDefenders.ElementAt(4).imagePath);
                                    }
                                    catch (Exception)
                                    {
                                        imgDefence5.Image = Properties.Resources.onbekend;
                                    }
                                    lblDefence5.Text = currentDefenders.ElementAt(4).firstName + " " + currentDefenders.ElementAt(4).lastName;

                                    imgDefence5.Visible = true;
                                    lblDefence5.Visible = true;
                                }
                            }
                        }
                    }
                }
            }

            if (frmAddPlayerToSelection.getSelection()[3] != null)
            {
                if (frmAddPlayerToSelection.getSelection()[3].Count() == 1)
                {
                    try
                    {
                        imgKeeper.Image = Image.FromFile(frmAddPlayerToSelection.getSelection()[3].ElementAt(0).imagePath);
                    }
                    catch (Exception)
                    {
                        imgKeeper.Image = Properties.Resources.onbekend;
                    }
                    
                    lblKeeper.Text = frmAddPlayerToSelection.getSelection()[3].ElementAt(0).firstName + " " + frmAddPlayerToSelection.getSelection()[3].ElementAt(0).lastName;

                    imgKeeper.Visible = true;
                    lblKeeper.Visible = true;
                }
            }
        }

        public frmMatchday()
        {
            InitializeComponent();

            attackers = new List<Player>();
            midfielders = new List<Player>();
            defenders = new List<Player>();
            keeper = new List<Player>();

            attackersTemp = new List<Player>();
            midfieldersTemp = new List<Player>();
            defendersTemp = new List<Player>();
            keeperTemp = new List<Player>();

            attackerCounter = 0;
            midfielderCounter = 0;
            defenderCounter = 0;

            selectionCounter = 0;

            foreach (Player player in Player.getAll())
            {
                if (player.favoritePosition == "Aanvaller" || player.alternativePosition == "Aanvaller")
                {
                    attackers.Add(player);
                }
                if (player.favoritePosition == "Middenvelder" || player.alternativePosition == "Middenvelder")
                {
                    midfielders.Add(player);
                }
                if (player.favoritePosition == "Verdediger" || player.alternativePosition == "Verdediger")
                {
                    defenders.Add(player);
                } 
                if (player.favoritePosition == "Keeper" || player.alternativePosition == "Keeper") { 
                    keeper.Add(player);
                } 
            }

            foreach (TrainingKey trainingKey in Training.getTrainingMap().Keys)
            {
                if (TrainingTotal.getTrainingTotalMap().ContainsKey(trainingKey.id))
                {
                    TrainingTotal.getTrainingTotalMap()[trainingKey.id].effort += Training.getTrainingMap()[trainingKey].effort;
                    TrainingTotal.getTrainingTotalMap()[trainingKey.id].attitude += Training.getTrainingMap()[trainingKey].attitude;
                    TrainingTotal.getTrainingTotalMap()[trainingKey.id].shape += Training.getTrainingMap()[trainingKey].shape;
                    TrainingTotal.getTrainingTotalMap()[trainingKey.id].total += 1;
                    return;
                }
                TrainingTotal.getTrainingTotalMap().Add(trainingKey.id, new TrainingTotal(1, Training.getTrainingMap()[trainingKey].attitude, Training.getTrainingMap()[trainingKey].effort, Training.getTrainingMap()[trainingKey].shape));
            }
        }

        private void btnAttack_Click(object sender, EventArgs e)
        {
            if (attackers.Count() == 0)
            {
                MessageBox.Show("U heeft geen aanvallers in uw selectie", "Geen aanvallers", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            foreach (Player player in attackersTemp)
            {
                selection.Remove(player);
            }
            selectionCounter -= attackerCounter;
            attackers.AddRange(attackersTemp);
            new frmAddPlayerToSelection(attackers, "Aanvaller", dtpDate.Value.ToShortDateString()).Show();
            attackerCounter = 0;

            foreach (Player player in attackersTemp)
            {
                MatchData.getMatchDataList().Remove(new MatchData(player.id, "Aanvaller"));
            }
            
            imgAttack1.Visible = false;
            lblAttack1.Visible = false;
            imgAttack2.Visible = false;
            lblAttack2.Visible = false;
            imgAttack3.Visible = false;
            lblAttack3.Visible = false;
            imgAttack4.Visible = false;
            lblAttack4.Visible = false;
        }

        private void btnSelectionFinished_Click(object sender, EventArgs e)
        {
            if (imgKeeper.Visible && selectionCounter == 10)
            {
                try
                {
                    MatchDA.addMatchData(MatchData.getMatchDataList(), dtpDate.Value.ToShortDateString());
                    MessageBox.Show("Gegevens zijn succesvol opgeslagen", "Gegevens verstuurd naar de database", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                catch (Exception exception)
                {
                    MessageBox.Show("Foutmelding: " + exception);
                }
                return;
            }
            MessageBox.Show("De opstelling is nog niet compleet", "Opstelling niet volledig", MessageBoxButtons.OK, MessageBoxIcon.Warning);
        }
    }
}
