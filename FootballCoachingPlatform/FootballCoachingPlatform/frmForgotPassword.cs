﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using FootballCoachingPlatform.DA;
using System.Net;
using System.Net.Mail;

namespace FootballCoachingPlatform
{
    public partial class frmForgotPassword : Form
    {
        public frmForgotPassword()
        {
            InitializeComponent();
        }

        private void btnRepairPassword_Click(object sender, EventArgs e)
        {
            String password = CoachDA.getPassword(txtMail.Text);

            if (password == String.Empty)
            {
                MessageBox.Show("Het e-mail adres " + txtMail.Text + " bestaat niet.", "Ongeldig mailadres", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtMail.Clear();
                txtMail.Focus();
                return;
            }

            try
            {
                this.sendmail(txtMail.Text, password);
            } catch (Exception exception)
            {
                MessageBox.Show("Foutmelding: " + exception);
            }
            
        }

        private void sendmail(String to, String password)
        {
            try
            {
                System.Net.Mail.MailMessage message = new MailMessage();

                message.To.Add(to);
                message.Subject = "Wachtwoord FootballCoachingPlatform";
                message.From = new MailAddress("myanonymousbotmail@gmail.com");
                message.Body = "Jouw wachtwoord is: " + password;

                SmtpClient client = new System.Net.Mail.SmtpClient("smtp.gmail.com", 587);

                client.Credentials = new NetworkCredential("myanonymousbotmail@gmail.com", "myanomail999");
                client.EnableSsl = true;

                client.Send(message);
            } catch (Exception exception)
            {
                MessageBox.Show("Foutmelding bij het verzenden van de e-mail. " + exception.Message);
            }        
        }

    }
}
