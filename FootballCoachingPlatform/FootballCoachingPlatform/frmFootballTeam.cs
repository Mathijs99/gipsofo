﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using FootballCoachingPlatform.Model;

namespace FootballCoachingPlatform
{
    public partial class frmFootballTeam : Form
    {
        public frmFootballTeam()
        {

            InitializeComponent();

            if (Team.name != null)
            {    
                lblTeamName.Text = Team.name;

                if (Team.imagePath != String.Empty)
                {
                    imgCreateTeam.Image = Image.FromFile(Team.imagePath);
                }
            }
        }

        private void imgCreateTeam_Click(object sender, EventArgs e)
        {

            if (Team.name != null)
            {
                this.Hide();
                new frmMyFootballTeam().ShowDialog();
                this.Close();
                return;
            }

            this.Hide();
            new frmAddTeam().ShowDialog();
            this.Close();
        }
    }
}
