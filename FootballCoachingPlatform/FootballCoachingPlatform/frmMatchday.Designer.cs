﻿namespace FootballCoachingPlatform
{
    partial class frmMatchday
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnAttack = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.btnMidfield = new System.Windows.Forms.Button();
            this.btnDefence = new System.Windows.Forms.Button();
            this.btnKeeper = new System.Windows.Forms.Button();
            this.imgAttack1 = new System.Windows.Forms.PictureBox();
            this.imgMidfield1 = new System.Windows.Forms.PictureBox();
            this.imgAttack4 = new System.Windows.Forms.PictureBox();
            this.imgAttack3 = new System.Windows.Forms.PictureBox();
            this.imgAttack2 = new System.Windows.Forms.PictureBox();
            this.imgDefence2 = new System.Windows.Forms.PictureBox();
            this.imgDefence3 = new System.Windows.Forms.PictureBox();
            this.imgDefence1 = new System.Windows.Forms.PictureBox();
            this.imgMidfield5 = new System.Windows.Forms.PictureBox();
            this.imgMidfield4 = new System.Windows.Forms.PictureBox();
            this.imgMidfield3 = new System.Windows.Forms.PictureBox();
            this.imgMidfield2 = new System.Windows.Forms.PictureBox();
            this.imgDefence4 = new System.Windows.Forms.PictureBox();
            this.imgDefence5 = new System.Windows.Forms.PictureBox();
            this.imgKeeper = new System.Windows.Forms.PictureBox();
            this.lblAttack1 = new System.Windows.Forms.Label();
            this.lblAttack2 = new System.Windows.Forms.Label();
            this.lblDefence1 = new System.Windows.Forms.Label();
            this.lblMidfield5 = new System.Windows.Forms.Label();
            this.lblMidfield4 = new System.Windows.Forms.Label();
            this.lblMidfield3 = new System.Windows.Forms.Label();
            this.lblMidfield2 = new System.Windows.Forms.Label();
            this.lblMidfield1 = new System.Windows.Forms.Label();
            this.lblAttack4 = new System.Windows.Forms.Label();
            this.lblAttack3 = new System.Windows.Forms.Label();
            this.lblDefence3 = new System.Windows.Forms.Label();
            this.lblKeeper = new System.Windows.Forms.Label();
            this.lblDefence4 = new System.Windows.Forms.Label();
            this.lblDefence5 = new System.Windows.Forms.Label();
            this.lblDefence2 = new System.Windows.Forms.Label();
            this.btnSelectionFinished = new System.Windows.Forms.Button();
            this.dtpDate = new System.Windows.Forms.DateTimePicker();
            ((System.ComponentModel.ISupportInitialize)(this.imgAttack1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgMidfield1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgAttack4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgAttack3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgAttack2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgDefence2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgDefence3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgDefence1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgMidfield5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgMidfield4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgMidfield3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgMidfield2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgDefence4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgDefence5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgKeeper)).BeginInit();
            this.SuspendLayout();
            // 
            // btnAttack
            // 
            this.btnAttack.BackColor = System.Drawing.Color.Crimson;
            this.btnAttack.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAttack.ForeColor = System.Drawing.Color.LawnGreen;
            this.btnAttack.Location = new System.Drawing.Point(1084, 53);
            this.btnAttack.Name = "btnAttack";
            this.btnAttack.Size = new System.Drawing.Size(193, 39);
            this.btnAttack.TabIndex = 0;
            this.btnAttack.Text = "Kies aanvallers";
            this.btnAttack.UseVisualStyleBackColor = false;
            this.btnAttack.Click += new System.EventHandler(this.btnAttack_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label2.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(12, 219);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(238, 39);
            this.label2.TabIndex = 4;
            this.label2.Text = "MIDDENVELD";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label3.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(12, 385);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(252, 39);
            this.label3.TabIndex = 5;
            this.label3.Text = "VERDEDIGING";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label4.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(12, 552);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(152, 39);
            this.label4.TabIndex = 6;
            this.label4.Text = "KEEPER";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label1.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 53);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(155, 39);
            this.label1.TabIndex = 4;
            this.label1.Text = "AANVAL";
            // 
            // btnMidfield
            // 
            this.btnMidfield.BackColor = System.Drawing.Color.Crimson;
            this.btnMidfield.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMidfield.ForeColor = System.Drawing.Color.LawnGreen;
            this.btnMidfield.Location = new System.Drawing.Point(1084, 219);
            this.btnMidfield.Name = "btnMidfield";
            this.btnMidfield.Size = new System.Drawing.Size(193, 39);
            this.btnMidfield.TabIndex = 10;
            this.btnMidfield.Text = "Kies middenvelders";
            this.btnMidfield.UseVisualStyleBackColor = false;
            this.btnMidfield.Click += new System.EventHandler(this.btnMidfield_Click);
            // 
            // btnDefence
            // 
            this.btnDefence.BackColor = System.Drawing.Color.Crimson;
            this.btnDefence.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDefence.ForeColor = System.Drawing.Color.LawnGreen;
            this.btnDefence.Location = new System.Drawing.Point(1084, 423);
            this.btnDefence.Name = "btnDefence";
            this.btnDefence.Size = new System.Drawing.Size(193, 39);
            this.btnDefence.TabIndex = 11;
            this.btnDefence.Text = "Kies verdedigers";
            this.btnDefence.UseVisualStyleBackColor = false;
            this.btnDefence.Click += new System.EventHandler(this.btnDefence_Click);
            // 
            // btnKeeper
            // 
            this.btnKeeper.BackColor = System.Drawing.Color.Crimson;
            this.btnKeeper.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnKeeper.ForeColor = System.Drawing.Color.LawnGreen;
            this.btnKeeper.Location = new System.Drawing.Point(1084, 552);
            this.btnKeeper.Name = "btnKeeper";
            this.btnKeeper.Size = new System.Drawing.Size(193, 39);
            this.btnKeeper.TabIndex = 12;
            this.btnKeeper.Text = "Kies keeper";
            this.btnKeeper.UseVisualStyleBackColor = false;
            this.btnKeeper.Click += new System.EventHandler(this.btnKeeper_Click);
            // 
            // imgAttack1
            // 
            this.imgAttack1.Location = new System.Drawing.Point(301, 53);
            this.imgAttack1.Name = "imgAttack1";
            this.imgAttack1.Size = new System.Drawing.Size(100, 77);
            this.imgAttack1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.imgAttack1.TabIndex = 13;
            this.imgAttack1.TabStop = false;
            this.imgAttack1.Visible = false;
            // 
            // imgMidfield1
            // 
            this.imgMidfield1.Location = new System.Drawing.Point(301, 219);
            this.imgMidfield1.Name = "imgMidfield1";
            this.imgMidfield1.Size = new System.Drawing.Size(100, 77);
            this.imgMidfield1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.imgMidfield1.TabIndex = 14;
            this.imgMidfield1.TabStop = false;
            this.imgMidfield1.Visible = false;
            // 
            // imgAttack4
            // 
            this.imgAttack4.Location = new System.Drawing.Point(785, 53);
            this.imgAttack4.Name = "imgAttack4";
            this.imgAttack4.Size = new System.Drawing.Size(100, 77);
            this.imgAttack4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.imgAttack4.TabIndex = 15;
            this.imgAttack4.TabStop = false;
            this.imgAttack4.Visible = false;
            // 
            // imgAttack3
            // 
            this.imgAttack3.Location = new System.Drawing.Point(626, 53);
            this.imgAttack3.Name = "imgAttack3";
            this.imgAttack3.Size = new System.Drawing.Size(100, 77);
            this.imgAttack3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.imgAttack3.TabIndex = 16;
            this.imgAttack3.TabStop = false;
            this.imgAttack3.Visible = false;
            // 
            // imgAttack2
            // 
            this.imgAttack2.Location = new System.Drawing.Point(470, 53);
            this.imgAttack2.Name = "imgAttack2";
            this.imgAttack2.Size = new System.Drawing.Size(100, 77);
            this.imgAttack2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.imgAttack2.TabIndex = 17;
            this.imgAttack2.TabStop = false;
            this.imgAttack2.Visible = false;
            // 
            // imgDefence2
            // 
            this.imgDefence2.Location = new System.Drawing.Point(470, 385);
            this.imgDefence2.Name = "imgDefence2";
            this.imgDefence2.Size = new System.Drawing.Size(100, 77);
            this.imgDefence2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.imgDefence2.TabIndex = 18;
            this.imgDefence2.TabStop = false;
            this.imgDefence2.Visible = false;
            // 
            // imgDefence3
            // 
            this.imgDefence3.Location = new System.Drawing.Point(626, 385);
            this.imgDefence3.Name = "imgDefence3";
            this.imgDefence3.Size = new System.Drawing.Size(100, 77);
            this.imgDefence3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.imgDefence3.TabIndex = 19;
            this.imgDefence3.TabStop = false;
            this.imgDefence3.Visible = false;
            // 
            // imgDefence1
            // 
            this.imgDefence1.Location = new System.Drawing.Point(301, 385);
            this.imgDefence1.Name = "imgDefence1";
            this.imgDefence1.Size = new System.Drawing.Size(100, 77);
            this.imgDefence1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.imgDefence1.TabIndex = 20;
            this.imgDefence1.TabStop = false;
            this.imgDefence1.Visible = false;
            // 
            // imgMidfield5
            // 
            this.imgMidfield5.Location = new System.Drawing.Point(928, 219);
            this.imgMidfield5.Name = "imgMidfield5";
            this.imgMidfield5.Size = new System.Drawing.Size(100, 77);
            this.imgMidfield5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.imgMidfield5.TabIndex = 21;
            this.imgMidfield5.TabStop = false;
            this.imgMidfield5.Visible = false;
            // 
            // imgMidfield4
            // 
            this.imgMidfield4.Location = new System.Drawing.Point(785, 219);
            this.imgMidfield4.Name = "imgMidfield4";
            this.imgMidfield4.Size = new System.Drawing.Size(100, 77);
            this.imgMidfield4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.imgMidfield4.TabIndex = 22;
            this.imgMidfield4.TabStop = false;
            this.imgMidfield4.Visible = false;
            // 
            // imgMidfield3
            // 
            this.imgMidfield3.Location = new System.Drawing.Point(626, 219);
            this.imgMidfield3.Name = "imgMidfield3";
            this.imgMidfield3.Size = new System.Drawing.Size(100, 77);
            this.imgMidfield3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.imgMidfield3.TabIndex = 23;
            this.imgMidfield3.TabStop = false;
            this.imgMidfield3.Visible = false;
            // 
            // imgMidfield2
            // 
            this.imgMidfield2.Location = new System.Drawing.Point(470, 219);
            this.imgMidfield2.Name = "imgMidfield2";
            this.imgMidfield2.Size = new System.Drawing.Size(100, 77);
            this.imgMidfield2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.imgMidfield2.TabIndex = 24;
            this.imgMidfield2.TabStop = false;
            this.imgMidfield2.Visible = false;
            // 
            // imgDefence4
            // 
            this.imgDefence4.Location = new System.Drawing.Point(785, 385);
            this.imgDefence4.Name = "imgDefence4";
            this.imgDefence4.Size = new System.Drawing.Size(100, 77);
            this.imgDefence4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.imgDefence4.TabIndex = 25;
            this.imgDefence4.TabStop = false;
            this.imgDefence4.Visible = false;
            // 
            // imgDefence5
            // 
            this.imgDefence5.Location = new System.Drawing.Point(928, 385);
            this.imgDefence5.Name = "imgDefence5";
            this.imgDefence5.Size = new System.Drawing.Size(100, 77);
            this.imgDefence5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.imgDefence5.TabIndex = 26;
            this.imgDefence5.TabStop = false;
            this.imgDefence5.Visible = false;
            // 
            // imgKeeper
            // 
            this.imgKeeper.Location = new System.Drawing.Point(301, 552);
            this.imgKeeper.Name = "imgKeeper";
            this.imgKeeper.Size = new System.Drawing.Size(100, 77);
            this.imgKeeper.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.imgKeeper.TabIndex = 27;
            this.imgKeeper.TabStop = false;
            this.imgKeeper.Visible = false;
            // 
            // lblAttack1
            // 
            this.lblAttack1.AutoSize = true;
            this.lblAttack1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAttack1.ForeColor = System.Drawing.Color.Black;
            this.lblAttack1.Location = new System.Drawing.Point(297, 26);
            this.lblAttack1.Name = "lblAttack1";
            this.lblAttack1.Size = new System.Drawing.Size(0, 24);
            this.lblAttack1.TabIndex = 28;
            // 
            // lblAttack2
            // 
            this.lblAttack2.AutoSize = true;
            this.lblAttack2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAttack2.ForeColor = System.Drawing.Color.Black;
            this.lblAttack2.Location = new System.Drawing.Point(466, 26);
            this.lblAttack2.Name = "lblAttack2";
            this.lblAttack2.Size = new System.Drawing.Size(0, 24);
            this.lblAttack2.TabIndex = 29;
            // 
            // lblDefence1
            // 
            this.lblDefence1.AutoSize = true;
            this.lblDefence1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDefence1.ForeColor = System.Drawing.Color.Black;
            this.lblDefence1.Location = new System.Drawing.Point(297, 358);
            this.lblDefence1.Name = "lblDefence1";
            this.lblDefence1.Size = new System.Drawing.Size(0, 24);
            this.lblDefence1.TabIndex = 30;
            // 
            // lblMidfield5
            // 
            this.lblMidfield5.AutoSize = true;
            this.lblMidfield5.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMidfield5.ForeColor = System.Drawing.Color.Black;
            this.lblMidfield5.Location = new System.Drawing.Point(924, 192);
            this.lblMidfield5.Name = "lblMidfield5";
            this.lblMidfield5.Size = new System.Drawing.Size(0, 24);
            this.lblMidfield5.TabIndex = 31;
            // 
            // lblMidfield4
            // 
            this.lblMidfield4.AutoSize = true;
            this.lblMidfield4.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMidfield4.ForeColor = System.Drawing.Color.Black;
            this.lblMidfield4.Location = new System.Drawing.Point(781, 192);
            this.lblMidfield4.Name = "lblMidfield4";
            this.lblMidfield4.Size = new System.Drawing.Size(0, 24);
            this.lblMidfield4.TabIndex = 32;
            // 
            // lblMidfield3
            // 
            this.lblMidfield3.AutoSize = true;
            this.lblMidfield3.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMidfield3.ForeColor = System.Drawing.Color.Black;
            this.lblMidfield3.Location = new System.Drawing.Point(622, 192);
            this.lblMidfield3.Name = "lblMidfield3";
            this.lblMidfield3.Size = new System.Drawing.Size(0, 24);
            this.lblMidfield3.TabIndex = 33;
            // 
            // lblMidfield2
            // 
            this.lblMidfield2.AutoSize = true;
            this.lblMidfield2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMidfield2.ForeColor = System.Drawing.Color.Black;
            this.lblMidfield2.Location = new System.Drawing.Point(466, 192);
            this.lblMidfield2.Name = "lblMidfield2";
            this.lblMidfield2.Size = new System.Drawing.Size(0, 24);
            this.lblMidfield2.TabIndex = 34;
            // 
            // lblMidfield1
            // 
            this.lblMidfield1.AutoSize = true;
            this.lblMidfield1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMidfield1.ForeColor = System.Drawing.Color.Black;
            this.lblMidfield1.Location = new System.Drawing.Point(297, 192);
            this.lblMidfield1.Name = "lblMidfield1";
            this.lblMidfield1.Size = new System.Drawing.Size(0, 24);
            this.lblMidfield1.TabIndex = 35;
            // 
            // lblAttack4
            // 
            this.lblAttack4.AutoSize = true;
            this.lblAttack4.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAttack4.ForeColor = System.Drawing.Color.Black;
            this.lblAttack4.Location = new System.Drawing.Point(781, 26);
            this.lblAttack4.Name = "lblAttack4";
            this.lblAttack4.Size = new System.Drawing.Size(0, 24);
            this.lblAttack4.TabIndex = 36;
            // 
            // lblAttack3
            // 
            this.lblAttack3.AutoSize = true;
            this.lblAttack3.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAttack3.ForeColor = System.Drawing.Color.Black;
            this.lblAttack3.Location = new System.Drawing.Point(622, 26);
            this.lblAttack3.Name = "lblAttack3";
            this.lblAttack3.Size = new System.Drawing.Size(0, 24);
            this.lblAttack3.TabIndex = 37;
            // 
            // lblDefence3
            // 
            this.lblDefence3.AutoSize = true;
            this.lblDefence3.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDefence3.ForeColor = System.Drawing.Color.Black;
            this.lblDefence3.Location = new System.Drawing.Point(622, 358);
            this.lblDefence3.Name = "lblDefence3";
            this.lblDefence3.Size = new System.Drawing.Size(0, 24);
            this.lblDefence3.TabIndex = 38;
            // 
            // lblKeeper
            // 
            this.lblKeeper.AutoSize = true;
            this.lblKeeper.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblKeeper.ForeColor = System.Drawing.Color.Black;
            this.lblKeeper.Location = new System.Drawing.Point(297, 525);
            this.lblKeeper.Name = "lblKeeper";
            this.lblKeeper.Size = new System.Drawing.Size(0, 24);
            this.lblKeeper.TabIndex = 39;
            // 
            // lblDefence4
            // 
            this.lblDefence4.AutoSize = true;
            this.lblDefence4.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDefence4.ForeColor = System.Drawing.Color.Black;
            this.lblDefence4.Location = new System.Drawing.Point(781, 358);
            this.lblDefence4.Name = "lblDefence4";
            this.lblDefence4.Size = new System.Drawing.Size(0, 24);
            this.lblDefence4.TabIndex = 40;
            // 
            // lblDefence5
            // 
            this.lblDefence5.AutoSize = true;
            this.lblDefence5.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDefence5.ForeColor = System.Drawing.Color.Black;
            this.lblDefence5.Location = new System.Drawing.Point(924, 358);
            this.lblDefence5.Name = "lblDefence5";
            this.lblDefence5.Size = new System.Drawing.Size(0, 24);
            this.lblDefence5.TabIndex = 41;
            // 
            // lblDefence2
            // 
            this.lblDefence2.AutoSize = true;
            this.lblDefence2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDefence2.ForeColor = System.Drawing.Color.Black;
            this.lblDefence2.Location = new System.Drawing.Point(466, 358);
            this.lblDefence2.Name = "lblDefence2";
            this.lblDefence2.Size = new System.Drawing.Size(0, 24);
            this.lblDefence2.TabIndex = 42;
            // 
            // btnSelectionFinished
            // 
            this.btnSelectionFinished.BackColor = System.Drawing.Color.DarkBlue;
            this.btnSelectionFinished.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSelectionFinished.ForeColor = System.Drawing.Color.Turquoise;
            this.btnSelectionFinished.Location = new System.Drawing.Point(1084, 293);
            this.btnSelectionFinished.Name = "btnSelectionFinished";
            this.btnSelectionFinished.Size = new System.Drawing.Size(193, 89);
            this.btnSelectionFinished.TabIndex = 43;
            this.btnSelectionFinished.Text = "Selectie klaar";
            this.btnSelectionFinished.UseVisualStyleBackColor = false;
            this.btnSelectionFinished.Click += new System.EventHandler(this.btnSelectionFinished_Click);
            // 
            // dtpDate
            // 
            this.dtpDate.Location = new System.Drawing.Point(828, 12);
            this.dtpDate.Name = "dtpDate";
            this.dtpDate.Size = new System.Drawing.Size(200, 20);
            this.dtpDate.TabIndex = 44;
            // 
            // frmMatchday
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.AliceBlue;
            this.ClientSize = new System.Drawing.Size(1289, 675);
            this.Controls.Add(this.dtpDate);
            this.Controls.Add(this.btnSelectionFinished);
            this.Controls.Add(this.lblDefence2);
            this.Controls.Add(this.lblDefence5);
            this.Controls.Add(this.lblDefence4);
            this.Controls.Add(this.lblKeeper);
            this.Controls.Add(this.lblDefence3);
            this.Controls.Add(this.lblAttack3);
            this.Controls.Add(this.lblAttack4);
            this.Controls.Add(this.lblMidfield1);
            this.Controls.Add(this.lblMidfield2);
            this.Controls.Add(this.lblMidfield3);
            this.Controls.Add(this.lblMidfield4);
            this.Controls.Add(this.lblMidfield5);
            this.Controls.Add(this.lblDefence1);
            this.Controls.Add(this.lblAttack2);
            this.Controls.Add(this.lblAttack1);
            this.Controls.Add(this.imgKeeper);
            this.Controls.Add(this.imgDefence5);
            this.Controls.Add(this.imgDefence4);
            this.Controls.Add(this.imgMidfield2);
            this.Controls.Add(this.imgMidfield3);
            this.Controls.Add(this.imgMidfield4);
            this.Controls.Add(this.imgMidfield5);
            this.Controls.Add(this.imgDefence1);
            this.Controls.Add(this.imgDefence3);
            this.Controls.Add(this.imgDefence2);
            this.Controls.Add(this.imgAttack2);
            this.Controls.Add(this.imgAttack3);
            this.Controls.Add(this.imgAttack4);
            this.Controls.Add(this.imgMidfield1);
            this.Controls.Add(this.imgAttack1);
            this.Controls.Add(this.btnKeeper);
            this.Controls.Add(this.btnDefence);
            this.Controls.Add(this.btnMidfield);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btnAttack);
            this.ForeColor = System.Drawing.Color.DarkSeaGreen;
            this.Name = "frmMatchday";
            this.Text = "Form2";
            this.Activated += new System.EventHandler(this.frmMatchday_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmMatchday_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.imgAttack1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgMidfield1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgAttack4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgAttack3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgAttack2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgDefence2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgDefence3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgDefence1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgMidfield5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgMidfield4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgMidfield3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgMidfield2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgDefence4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgDefence5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgKeeper)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnAttack;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnMidfield;
        private System.Windows.Forms.Button btnDefence;
        private System.Windows.Forms.Button btnKeeper;
        private System.Windows.Forms.PictureBox imgAttack1;
        private System.Windows.Forms.PictureBox imgMidfield1;
        private System.Windows.Forms.PictureBox imgAttack4;
        private System.Windows.Forms.PictureBox imgAttack3;
        private System.Windows.Forms.PictureBox imgAttack2;
        private System.Windows.Forms.PictureBox imgDefence2;
        private System.Windows.Forms.PictureBox imgDefence3;
        private System.Windows.Forms.PictureBox imgDefence1;
        private System.Windows.Forms.PictureBox imgMidfield5;
        private System.Windows.Forms.PictureBox imgMidfield4;
        private System.Windows.Forms.PictureBox imgMidfield3;
        private System.Windows.Forms.PictureBox imgMidfield2;
        private System.Windows.Forms.PictureBox imgDefence4;
        private System.Windows.Forms.PictureBox imgDefence5;
        private System.Windows.Forms.PictureBox imgKeeper;
        private System.Windows.Forms.Label lblAttack1;
        private System.Windows.Forms.Label lblAttack2;
        private System.Windows.Forms.Label lblDefence1;
        private System.Windows.Forms.Label lblMidfield5;
        private System.Windows.Forms.Label lblMidfield4;
        private System.Windows.Forms.Label lblMidfield3;
        private System.Windows.Forms.Label lblMidfield2;
        private System.Windows.Forms.Label lblMidfield1;
        private System.Windows.Forms.Label lblAttack4;
        private System.Windows.Forms.Label lblAttack3;
        private System.Windows.Forms.Label lblDefence3;
        private System.Windows.Forms.Label lblKeeper;
        private System.Windows.Forms.Label lblDefence4;
        private System.Windows.Forms.Label lblDefence5;
        private System.Windows.Forms.Label lblDefence2;
        private System.Windows.Forms.Button btnSelectionFinished;
        private System.Windows.Forms.DateTimePicker dtpDate;
    }

     
       
        
            
    
}