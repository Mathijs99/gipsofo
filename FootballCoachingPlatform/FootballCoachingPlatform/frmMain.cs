﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Collections;
using FootballCoachingPlatform.DA;
using FootballCoachingPlatform.Model;
using FootballCoachingPlatform.Helper;


namespace FootballCoachingPlatform
{
    public partial class frmMain : Form
    {
        private ArrayList myList;

        public frmMain()
        {
            InitializeComponent();
            loadImages();
            myTimer.Start();
        }

        private void myTimer_Tick(object sender, EventArgs e)
        {
            imgBanner.Image = (Image) myList[new Random().Next(0, 10)];
        }

        private void loadImages()
        {
            myList = new ArrayList();

            myList.Add(Properties.Resources.footballCoaching1);
            myList.Add(Properties.Resources.footballCoaching2);
            myList.Add(Properties.Resources.footballCoaching3);
            myList.Add(Properties.Resources.footballCoaching4);
            myList.Add(Properties.Resources.footballCoaching5);
            myList.Add(Properties.Resources.footballCoaching6);
            myList.Add(Properties.Resources.footballCoaching7);
            myList.Add(Properties.Resources.footballCoaching8);
            myList.Add(Properties.Resources.footballCoaching9);
            myList.Add(Properties.Resources.footballCoaching10);
        }

        private void lblForgotPassword_Click(object sender, EventArgs e)
        {
            new frmForgotPassword().ShowDialog();
        }

        private void lblNoAccount_Click(object sender, EventArgs e)
        {
            new frmNoAccount().ShowDialog();
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            String username = txtUsername.Text;
            String password = txtPassword.Text;

            try
            {
                if (CoachDA.login(username, password))
                {

                    Coach.username = username;
                    Coach.password = password;

                    Initializer.execute();

                    this.Hide();
                    new frmFootballTeam().ShowDialog();
                    this.Close();
                    return;
                }
            } catch (Exception exception)
            {
                MessageBox.Show("Niet mogelijk om met de database te verbinden: " + exception, "Database connectie gefaald", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
           
            MessageBox.Show("Het ingegeven wachwoord is onjuist. Herbekijk gebruikersnaam en wachtwoord.", "Onjuist wachtwoord", MessageBoxButtons.OK, MessageBoxIcon.Information);
            txtPassword.Clear();
            txtPassword.Focus();
        }

    }
}