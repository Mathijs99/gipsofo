﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using FootballCoachingPlatform.DA;
using FootballCoachingPlatform.Model;
using FootballCoachingPlatform.Helper;

namespace FootballCoachingPlatform
{
    public partial class frmAddTeam : Form
    {
        String path = "";

        public frmAddTeam()
        {
            InitializeComponent();
        }

        private void btnCreateTeam_Click(object sender, EventArgs e)
        {
            String name = txtTeamName.Text;
            String image = path;
            String coach = Coach.username;

            try
            {
                TeamDA.addTeam(name, image, coach);
                MessageBox.Show("Team succesvol toegevoegd", "Team toegevoegd", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.Hide();
                new frmMyFootballTeam().ShowDialog();
                this.Close();
            } catch (Exception exception)
            {
                MessageBox.Show("Foutmelding: " + exception, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btnUpload_Click(object sender, EventArgs e)
        {
            try
            {
                path = ImageManager.handle(imgPicture);
            } catch (Exception)
            {

            }
            
        }
    }
}
