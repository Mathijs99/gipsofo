﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using FootballCoachingPlatform.DA;
using FootballCoachingPlatform.Helper;
using System.Net.Mail;

namespace FootballCoachingPlatform
{
    public partial class frmNoAccount : Form
    {
        String imagePath = String.Empty;
        private bool firstCheck = true;

        public frmNoAccount()
        {
            InitializeComponent();
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (firstCheck)
            {
                firstCheck = false;
                System.Diagnostics.Process.Start("https://www.hln.be");
                chkTaC.Checked = false;
            }

        }

        private void btnMakeAccount_Click(object sender, EventArgs e)
        {
            if (!chkTaC.Checked)
            {
                MessageBox.Show("Algemene voorwaarden niet goed gegekeurd.", "Algemene voorwaarden", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            String username = txtUsername.Text;
            String firstname = txtFirstname.Text;
            String lastname = txtLastname.Text;

            try
            {
                new MailAddress(txtMail.Text);
            }
            catch (Exception)
            {
                MessageBox.Show("Geen geldig mail adres ingevoerd", "voer een geldig mail adres in", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            String mail = txtMail.Text;
            String password = txtPassword.Text;
            String repeatedPassword = txtRepeatedPassword.Text;

            if (checkIfEmpty(username, firstname, lastname, mail, password, repeatedPassword))
            {
                return;
            }


            //query om te kijken of de username al in de database zit
            if (CoachDA.checkIfUsernameExists(username))
            {
                MessageBox.Show("De gebruikersnaam \"" + username + "\" bestaat al. U moet een andere gebruikersnaam kiezen", "Gebruikersnaam", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtUsername.Clear();
                txtUsername.Focus();
                return;
            }

            if (CoachDA.checkIfMailExists(mail))
            {
                MessageBox.Show("Het e-mailadres \"" + mail + "\" is al in gebruik. U moet een ander e-mailadres kiezen", "E-mailadres", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtMail.Clear();
                txtMail.Focus();
                return;
            }

            if (password != repeatedPassword)
            {
                MessageBox.Show("Wachtwoorden komen niet overeen", "Wachtwoorden", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtPassword.Clear();
                txtRepeatedPassword.Clear();
                txtPassword.Focus();
                return;
            }

            CoachDA.addCoach(username, firstname, lastname, mail, password, imagePath);
            MessageBox.Show("Het account is succesvol aangemaakt.", "Account aangemaakt", MessageBoxButtons.OK, MessageBoxIcon.Information);
            this.Hide();
            new frmMain().ShowDialog();
            this.Close();

        }

        private bool checkIfEmpty(String username, String firstname, String lastname, String mail, String password, String repeatedPassword)
        {
            if (username == String.Empty)
            {
                MessageBox.Show("Gebruikersnaam niet ingvuld!", "Gebruikersnaam", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtUsername.Focus();
                return true;
            }

            if (firstname == String.Empty)
            {
                MessageBox.Show("Voornaam niet ingvuld!", "Voornaam", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtFirstname.Focus();
                return true;
            }

            if (lastname == String.Empty)
            {
                MessageBox.Show("Achternaam niet ingvuld!", "Achternaam", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtLastname.Focus();
                return true;
            }

            if (mail == String.Empty)
            {
                MessageBox.Show("E-mailadres niet ingvuld!", "E-mailadres", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtMail.Focus();
                return true;
            }

            if (password == String.Empty)
            {
                MessageBox.Show("Wachtwoord niet ingvuld!", "Wachtwoord", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtUsername.Focus();
                return true;
            }

            if (repeatedPassword == String.Empty)
            {
                MessageBox.Show("Herhaal wachtwoord niet ingvuld!", "Herhaal wachtwoord", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtRepeatedPassword.Focus();
                return true;
            }

            return false;
        }

        private void btnUploadImage_Click(object sender, EventArgs e)
        {
            imagePath = ImageManager.handle(imgImage);
        }


    }
}
