﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FootballCoachingPlatform.Model
{
    public class Player
    {
        public int id { get; set; }
        public String firstName { get; set; }
        public String lastName { get; set; }
        public int age { get; set; }
        public String favoritePosition { get; set; }
        public String alternativePosition { get; set; }
        public String imagePath { get; set; }

        private static List<Player> playerList = new List<Player>();

        public Player(int id, String firstName, String lastName, int age, String favoritePosition, String alternativePosition, String imagePath)
        {
            this.id = id;
            this.firstName = firstName;
            this.lastName = lastName;
            this.age = age;
            this.favoritePosition = favoritePosition;
            this.alternativePosition = alternativePosition;
            this.imagePath = imagePath;
        }

        public static List<Player> getAll()
        {
            return playerList;
        }

        public static Player getPlayerFromPlayerId(int id)
        {
            foreach (Player player in playerList)
            {
                if (player.id == id)
                {
                    return player;
                }
            }
            return null;
        }
    }
}
