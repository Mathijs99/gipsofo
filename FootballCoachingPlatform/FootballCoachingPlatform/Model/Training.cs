﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FootballCoachingPlatform.Model
{
    public class Training
    {
        public double attitude { get; private set; }
        public double effort { get; private set; }
        public double shape { get; private set; }
        public String comment { get; private set;  }

        private static Dictionary<TrainingKey, Training> trainingMap = new Dictionary<TrainingKey, Training>();

        public Training(double attitude, double effort, double shape, String comment)
        {
            this.attitude = attitude;
            this.effort = effort;
            this.shape = shape;
            this.comment = comment;
        }

        public static Dictionary<TrainingKey, Training> getTrainingMap()
        {
            return trainingMap;
        }
    }
}
