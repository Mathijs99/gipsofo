﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FootballCoachingPlatform.Model
{
    public class TrainingTotal
    {
        public int total { get; set; }
        public double attitude { get; set; }
        public double effort { get; set; }
        public double shape { get; set; }

        private static Dictionary<Int32, TrainingTotal> trainingTotalMap = new Dictionary<int, TrainingTotal>();

        public TrainingTotal(int total, double attitude, double effort, double shape)
        {
            this.total = total;
            this.attitude = attitude;
            this.effort = effort;
            this.shape = shape;
        }

        public static Dictionary<Int32, TrainingTotal> getTrainingTotalMap()
        {
            return trainingTotalMap;
        }
    }
}
