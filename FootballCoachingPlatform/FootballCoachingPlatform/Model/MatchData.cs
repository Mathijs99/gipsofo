﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FootballCoachingPlatform.Model
{
    public class MatchData
    {
        public int playerid { get; set; }
        public String position { get; set; }

        private static List<MatchData> matchDataList = new List<MatchData>();

        public MatchData(int playerid, String position)
        {
            this.playerid = playerid;
            this.position = position;
        }

        public static List<MatchData> getMatchDataList()
        {
            return matchDataList;
        }
    }
}
