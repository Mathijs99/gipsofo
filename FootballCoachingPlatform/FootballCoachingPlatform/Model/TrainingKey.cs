﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FootballCoachingPlatform.Model
{
    public class TrainingKey
    {
        public int id { get; private set; }
        public String date { get; private set; }

        public TrainingKey(int id, String date)
        {
            this.id = id;
            this.date = date;
        }
    }
}
