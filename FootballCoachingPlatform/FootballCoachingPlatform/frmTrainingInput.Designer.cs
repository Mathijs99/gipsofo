﻿namespace FootballCoachingPlatform
{
    partial class frmTrainingInput
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.imgCoach = new System.Windows.Forms.PictureBox();
            this.lblUsername = new System.Windows.Forms.Label();
            this.lblTeamName = new System.Windows.Forms.Label();
            this.btnNext = new System.Windows.Forms.Button();
            this.btnPrevious = new System.Windows.Forms.Button();
            this.imgPlayer = new System.Windows.Forms.PictureBox();
            this.lblPlayerName = new System.Windows.Forms.Label();
            this.chkPresent = new System.Windows.Forms.CheckBox();
            this.txtComment = new System.Windows.Forms.TextBox();
            this.lblComment = new System.Windows.Forms.Label();
            this.imgTeam = new System.Windows.Forms.PictureBox();
            this.btnSaveThisOne = new System.Windows.Forms.Button();
            this.txtShape = new System.Windows.Forms.TextBox();
            this.txtAttitude = new System.Windows.Forms.TextBox();
            this.txtEffort = new System.Windows.Forms.TextBox();
            this.lblAttitude = new System.Windows.Forms.Label();
            this.lblShape = new System.Windows.Forms.Label();
            this.lblEffort = new System.Windows.Forms.Label();
            this.lblPoints1 = new System.Windows.Forms.Label();
            this.lblPoints2 = new System.Windows.Forms.Label();
            this.lblPoints3 = new System.Windows.Forms.Label();
            this.dtpDate = new System.Windows.Forms.DateTimePicker();
            ((System.ComponentModel.ISupportInitialize)(this.imgCoach)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgPlayer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgTeam)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(388, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(283, 39);
            this.label1.TabIndex = 0;
            this.label1.Text = "Training invoeren";
            // 
            // imgCoach
            // 
            this.imgCoach.Location = new System.Drawing.Point(36, 12);
            this.imgCoach.Name = "imgCoach";
            this.imgCoach.Size = new System.Drawing.Size(99, 80);
            this.imgCoach.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.imgCoach.TabIndex = 1;
            this.imgCoach.TabStop = false;
            // 
            // lblUsername
            // 
            this.lblUsername.AutoSize = true;
            this.lblUsername.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblUsername.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUsername.Location = new System.Drawing.Point(148, 65);
            this.lblUsername.Name = "lblUsername";
            this.lblUsername.Size = new System.Drawing.Size(196, 27);
            this.lblUsername.TabIndex = 2;
            this.lblUsername.Text = "<Gebruikersnaam>";
            // 
            // lblTeamName
            // 
            this.lblTeamName.AutoSize = true;
            this.lblTeamName.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblTeamName.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTeamName.Location = new System.Drawing.Point(148, 12);
            this.lblTeamName.Name = "lblTeamName";
            this.lblTeamName.Size = new System.Drawing.Size(135, 27);
            this.lblTeamName.TabIndex = 3;
            this.lblTeamName.Text = "<Clubnaam>";
            // 
            // btnNext
            // 
            this.btnNext.Location = new System.Drawing.Point(701, 399);
            this.btnNext.Name = "btnNext";
            this.btnNext.Size = new System.Drawing.Size(75, 23);
            this.btnNext.TabIndex = 4;
            this.btnNext.Text = "Volgende";
            this.btnNext.UseVisualStyleBackColor = true;
            this.btnNext.Click += new System.EventHandler(this.btnNext_Click);
            // 
            // btnPrevious
            // 
            this.btnPrevious.Location = new System.Drawing.Point(36, 399);
            this.btnPrevious.Name = "btnPrevious";
            this.btnPrevious.Size = new System.Drawing.Size(75, 23);
            this.btnPrevious.TabIndex = 5;
            this.btnPrevious.Text = "Vorige";
            this.btnPrevious.UseVisualStyleBackColor = true;
            this.btnPrevious.Click += new System.EventHandler(this.btnPrevious_Click);
            // 
            // imgPlayer
            // 
            this.imgPlayer.Location = new System.Drawing.Point(36, 216);
            this.imgPlayer.Name = "imgPlayer";
            this.imgPlayer.Size = new System.Drawing.Size(120, 107);
            this.imgPlayer.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.imgPlayer.TabIndex = 6;
            this.imgPlayer.TabStop = false;
            // 
            // lblPlayerName
            // 
            this.lblPlayerName.AutoSize = true;
            this.lblPlayerName.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPlayerName.Location = new System.Drawing.Point(32, 195);
            this.lblPlayerName.Name = "lblPlayerName";
            this.lblPlayerName.Size = new System.Drawing.Size(121, 20);
            this.lblPlayerName.TabIndex = 7;
            this.lblPlayerName.Text = "<Spelersnaam>";
            // 
            // chkPresent
            // 
            this.chkPresent.AutoSize = true;
            this.chkPresent.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkPresent.Location = new System.Drawing.Point(33, 164);
            this.chkPresent.Name = "chkPresent";
            this.chkPresent.Size = new System.Drawing.Size(120, 28);
            this.chkPresent.TabIndex = 8;
            this.chkPresent.Text = "Aanwezig";
            this.chkPresent.UseVisualStyleBackColor = true;
            this.chkPresent.CheckedChanged += new System.EventHandler(this.chkPresent_CheckedChanged);
            // 
            // txtComment
            // 
            this.txtComment.Location = new System.Drawing.Point(510, 259);
            this.txtComment.Multiline = true;
            this.txtComment.Name = "txtComment";
            this.txtComment.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtComment.Size = new System.Drawing.Size(225, 64);
            this.txtComment.TabIndex = 9;
            // 
            // lblComment
            // 
            this.lblComment.AutoSize = true;
            this.lblComment.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblComment.Location = new System.Drawing.Point(579, 240);
            this.lblComment.Name = "lblComment";
            this.lblComment.Size = new System.Drawing.Size(156, 16);
            this.lblComment.TabIndex = 10;
            this.lblComment.Text = "Voeg een opmerking toe";
            // 
            // imgTeam
            // 
            this.imgTeam.Location = new System.Drawing.Point(677, 12);
            this.imgTeam.Name = "imgTeam";
            this.imgTeam.Size = new System.Drawing.Size(99, 80);
            this.imgTeam.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.imgTeam.TabIndex = 11;
            this.imgTeam.TabStop = false;
            // 
            // btnSaveThisOne
            // 
            this.btnSaveThisOne.BackColor = System.Drawing.Color.Maroon;
            this.btnSaveThisOne.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSaveThisOne.ForeColor = System.Drawing.Color.White;
            this.btnSaveThisOne.Location = new System.Drawing.Point(334, 358);
            this.btnSaveThisOne.Name = "btnSaveThisOne";
            this.btnSaveThisOne.Size = new System.Drawing.Size(163, 64);
            this.btnSaveThisOne.TabIndex = 12;
            this.btnSaveThisOne.Text = "Deze wijziging opslaan";
            this.btnSaveThisOne.UseVisualStyleBackColor = false;
            this.btnSaveThisOne.Click += new System.EventHandler(this.btnSaveThisOne_Click);
            // 
            // txtShape
            // 
            this.txtShape.Location = new System.Drawing.Point(597, 193);
            this.txtShape.Name = "txtShape";
            this.txtShape.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtShape.Size = new System.Drawing.Size(100, 20);
            this.txtShape.TabIndex = 13;
            // 
            // txtAttitude
            // 
            this.txtAttitude.Location = new System.Drawing.Point(313, 195);
            this.txtAttitude.Name = "txtAttitude";
            this.txtAttitude.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtAttitude.Size = new System.Drawing.Size(100, 20);
            this.txtAttitude.TabIndex = 14;
            // 
            // txtEffort
            // 
            this.txtEffort.Location = new System.Drawing.Point(313, 303);
            this.txtEffort.Name = "txtEffort";
            this.txtEffort.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtEffort.Size = new System.Drawing.Size(100, 20);
            this.txtEffort.TabIndex = 15;
            // 
            // lblAttitude
            // 
            this.lblAttitude.AutoSize = true;
            this.lblAttitude.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAttitude.Location = new System.Drawing.Point(231, 195);
            this.lblAttitude.Name = "lblAttitude";
            this.lblAttitude.Size = new System.Drawing.Size(76, 20);
            this.lblAttitude.TabIndex = 16;
            this.lblAttitude.Text = "Houding";
            // 
            // lblShape
            // 
            this.lblShape.AutoSize = true;
            this.lblShape.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblShape.Location = new System.Drawing.Point(540, 193);
            this.lblShape.Name = "lblShape";
            this.lblShape.Size = new System.Drawing.Size(51, 20);
            this.lblShape.TabIndex = 17;
            this.lblShape.Text = "Vorm";
            // 
            // lblEffort
            // 
            this.lblEffort.AutoSize = true;
            this.lblEffort.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEffort.Location = new System.Drawing.Point(257, 301);
            this.lblEffort.Name = "lblEffort";
            this.lblEffort.Size = new System.Drawing.Size(50, 20);
            this.lblEffort.TabIndex = 18;
            this.lblEffort.Text = "Inzet";
            // 
            // lblPoints1
            // 
            this.lblPoints1.AutoSize = true;
            this.lblPoints1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPoints1.Location = new System.Drawing.Point(419, 195);
            this.lblPoints1.Name = "lblPoints1";
            this.lblPoints1.Size = new System.Drawing.Size(31, 20);
            this.lblPoints1.TabIndex = 19;
            this.lblPoints1.Text = "/10";
            // 
            // lblPoints2
            // 
            this.lblPoints2.AutoSize = true;
            this.lblPoints2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPoints2.Location = new System.Drawing.Point(419, 303);
            this.lblPoints2.Name = "lblPoints2";
            this.lblPoints2.Size = new System.Drawing.Size(31, 20);
            this.lblPoints2.TabIndex = 20;
            this.lblPoints2.Text = "/10";
            // 
            // lblPoints3
            // 
            this.lblPoints3.AutoSize = true;
            this.lblPoints3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPoints3.Location = new System.Drawing.Point(703, 195);
            this.lblPoints3.Name = "lblPoints3";
            this.lblPoints3.Size = new System.Drawing.Size(31, 20);
            this.lblPoints3.TabIndex = 21;
            this.lblPoints3.Text = "/10";
            // 
            // dtpDate
            // 
            this.dtpDate.Location = new System.Drawing.Point(36, 118);
            this.dtpDate.Name = "dtpDate";
            this.dtpDate.Size = new System.Drawing.Size(215, 20);
            this.dtpDate.TabIndex = 22;
            // 
            // frmTrainingInput
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.dtpDate);
            this.Controls.Add(this.lblPoints3);
            this.Controls.Add(this.lblPoints2);
            this.Controls.Add(this.lblPoints1);
            this.Controls.Add(this.lblEffort);
            this.Controls.Add(this.lblShape);
            this.Controls.Add(this.lblAttitude);
            this.Controls.Add(this.txtEffort);
            this.Controls.Add(this.txtAttitude);
            this.Controls.Add(this.txtShape);
            this.Controls.Add(this.btnSaveThisOne);
            this.Controls.Add(this.imgTeam);
            this.Controls.Add(this.lblComment);
            this.Controls.Add(this.txtComment);
            this.Controls.Add(this.chkPresent);
            this.Controls.Add(this.lblPlayerName);
            this.Controls.Add(this.imgPlayer);
            this.Controls.Add(this.btnPrevious);
            this.Controls.Add(this.btnNext);
            this.Controls.Add(this.lblTeamName);
            this.Controls.Add(this.lblUsername);
            this.Controls.Add(this.imgCoach);
            this.Controls.Add(this.label1);
            this.Name = "frmTrainingInput";
            this.Text = "Training";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmTrainingInput_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.imgCoach)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgPlayer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgTeam)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }



        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox imgCoach;
        private System.Windows.Forms.Label lblUsername;
        private System.Windows.Forms.Label lblTeamName;
        private System.Windows.Forms.Button btnNext;
        private System.Windows.Forms.Button btnPrevious;
        private System.Windows.Forms.PictureBox imgPlayer;
        private System.Windows.Forms.Label lblPlayerName;
        private System.Windows.Forms.CheckBox chkPresent;
        private System.Windows.Forms.TextBox txtComment;
        private System.Windows.Forms.Label lblComment;
        private System.Windows.Forms.PictureBox imgTeam;
        private System.Windows.Forms.Button btnSaveThisOne;
        private System.Windows.Forms.TextBox txtShape;
        private System.Windows.Forms.TextBox txtAttitude;
        private System.Windows.Forms.TextBox txtEffort;
        private System.Windows.Forms.Label lblAttitude;
        private System.Windows.Forms.Label lblShape;
        private System.Windows.Forms.Label lblEffort;
        private System.Windows.Forms.Label lblPoints1;
        private System.Windows.Forms.Label lblPoints2;
        private System.Windows.Forms.Label lblPoints3;
        private System.Windows.Forms.DateTimePicker dtpDate;
    }

       
}
#endregion