﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using FootballCoachingPlatform.Model;
using FootballCoachingPlatform.Helper;
using FootballCoachingPlatform.DA;

namespace FootballCoachingPlatform
{
    public partial class frmMyTeam : Form
    {
        private int counter;
        private String imagePath;
        private Player currentPlayer;

        public frmMyTeam()
        {
            InitializeComponent();

            counter = 0;
            imagePath = String.Empty;

            lblTeamName.Text = Team.name;
            lblUsername.Text = Coach.username;

            cboFavoritePosition.Items.Add("Aanvaller");
            cboFavoritePosition.Items.Add("Middenvelder");
            cboFavoritePosition.Items.Add("Verdediger");
            cboFavoritePosition.Items.Add("Keeper");
            cboAlternativePosition.Items.Add("Aanvaller");
            cboAlternativePosition.Items.Add("Middenvelder");
            cboAlternativePosition.Items.Add("Verdediger");
            cboAlternativePosition.Items.Add("Keeper");
            cboAlternativePosition.Items.Add("Geen");

            if (Player.getAll().Count() == 0)
            {
                MessageBox.Show("Nog geen spelers", "Eerst spelers toevoegen", MessageBoxButtons.OK, MessageBoxIcon.Information);
                new frmAddPlayer().ShowDialog();
                return;
            }

            loadPlayer(Player.getAll().ElementAt(counter));

            try
            {
                imgCoach.Image = Image.FromFile(Coach.imagePath);
            }
            catch (Exception)
            {
                imgCoach.Image = Properties.Resources.onbekend;
            }

            try
            {
                imgTeam.Image = Image.FromFile(Team.imagePath);
            }
            catch (Exception)
            {
                imgTeam.Image = Properties.Resources.onbekend;
            }
        }

        private void loadPlayer(Player player)
        {
            txtAge.Text = player.age.ToString();
            cboAlternativePosition.Text = player.alternativePosition;
            cboFavoritePosition.Text = player.favoritePosition;
            txtFirstname.Text = player.firstName;
            txtLastname.Text = player.lastName;

            try
            {
                imgPlayer.Image = Image.FromFile(player.imagePath);
            } catch (Exception)
            {
                imgPlayer.Image = Properties.Resources.onbekend;
            }
            
            currentPlayer = player;
            imagePath = currentPlayer.imagePath;
        }

        private void btnAddPlayer_Click(object sender, EventArgs e)
        {
            new frmAddPlayer().ShowDialog();
        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            if (Player.getAll().Count() <= 1)
            {
                return;
            }

            counter++;

            if (counter == Player.getAll().Count())
            {
                counter = 0;
            }

            loadPlayer(Player.getAll().ElementAt(counter));
        }

        private void btnPrevious_Click(object sender, EventArgs e)
        {
            if (Player.getAll().Count() <= 1)
            {
                return;
            }

            counter--;

            if (counter == -1)
            {
                counter = Player.getAll().Count() - 1;
            }

            loadPlayer(Player.getAll().ElementAt(counter));
        }

        private void imgPlayer_Click(object sender, EventArgs e)
        {
            imagePath = ImageManager.handle(imgPlayer);
        }

        private void btnRemove_Click(object sender, EventArgs e)
        {
            try
            {
                PlayerDA.removePlayer(currentPlayer);
            } catch (Exception exception)
            {
                MessageBox.Show("Foutmelding: " + exception);
                return;
            }

            MessageBox.Show("Speler succesvol verwijderd uit de database", "Speler verwijderd", MessageBoxButtons.OK, MessageBoxIcon.Information);

            if (Player.getAll().Count() == 0)
            {
                txtFirstname.Text = String.Empty;
                txtLastname.Text = String.Empty;
                cboFavoritePosition.Text = String.Empty;
                cboAlternativePosition.Text = String.Empty;
                txtAge.Text = String.Empty;
                imgPlayer.Image = Properties.Resources.onbekend;
                return;

            }

            if (counter == Player.getAll().Count())
            {
                counter = 0;
            }

            loadPlayer(Player.getAll().ElementAt(counter));


        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            int age;
            bool isNumeric = Int32.TryParse(txtAge.Text, out age);

            if (!isNumeric)
            {
                MessageBox.Show(txtAge.Text + " is geen getal", "Geen getal ingevuld", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtAge.Text = String.Empty;
                txtAge.Focus();
                return;
            }

            if (cboAlternativePosition.Text == cboFavoritePosition.Text)
            {
                MessageBox.Show("Favoriete positie en nevenpositie mogen niet hetzelfde zijn", "Foutmelding", MessageBoxButtons.OK, MessageBoxIcon.Error);
                cboAlternativePosition.SelectedIndex = 0;
                cboFavoritePosition.SelectedIndex = 0;
                cboAlternativePosition.Focus();
                return;
            }

            try
            {
                PlayerDA.changePlayerData(currentPlayer, txtFirstname.Text, txtLastname.Text, age, cboFavoritePosition.Text, cboAlternativePosition.Text, imagePath);
            } catch (Exception exception)
            {
                MessageBox.Show("Foutmelding: " + exception);
                return;
            }

            MessageBox.Show("De gegevens van de speler zijn succesvol gewijzigd", "Speler data gewijzegd", MessageBoxButtons.OK, MessageBoxIcon.Information);

        }

        private void frmMyTeam_Activated(object sender, EventArgs e)
        {
            this.loadPlayer(Player.getAll().ElementAt(0));
        }
    }
}
