﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using FootballCoachingPlatform.Model;
using FootballCoachingPlatform.DA;

namespace FootballCoachingPlatform
{
    public partial class frmTrainingInput : Form
    {
        private int counter;
        private Dictionary<TrainingKey, Training> currentTrainingMap;
        private Player currentPlayer;

        public frmTrainingInput()
        {
            InitializeComponent();

            counter = 0;
            currentTrainingMap = new Dictionary<TrainingKey, Training>();
            this.loadPlayer(Player.getAll().ElementAt(counter));

            lblTeamName.Text = Team.name;
            lblUsername.Text = Coach.username;

            try
            {
                imgCoach.Image = Image.FromFile(Coach.imagePath);
            }
            catch (Exception)
            {
                imgCoach.Image = Properties.Resources.onbekend;
            }

            try
            {
                imgTeam.Image = Image.FromFile(Team.imagePath);
            }
            catch (Exception)
            {
                imgTeam.Image = Properties.Resources.onbekend;
            }
            Player player;
            try
            {
                player = Player.getAll().ElementAt(counter);
            } catch (Exception es)
            {
                MessageBox.Show(
                    es + "");
                return;
            }
            

            lblPlayerName.Text = player.firstName + " " + player.lastName;
            try
            {
                imgPlayer.Image = Image.FromFile(player.imagePath);
            } catch (Exception)
            {
                imgPlayer.Image = Properties.Resources.onbekend;
            }
            

            txtAttitude.Visible = false;
            txtComment.Visible = false;
            txtEffort.Visible = false;
            txtShape.Visible = false;
            lblAttitude.Visible = false;
            lblComment.Visible = false;
            lblEffort.Visible = false;
            lblShape.Visible = false;
            lblPoints1.Visible = false;
            lblPoints2.Visible = false;
            lblPoints3.Visible = false;
        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            counter++;

            if (counter == Player.getAll().Count())
            {
                counter = 0;
            }

            this.loadPlayer(Player.getAll().ElementAt(counter));
        }

        private void btnPrevious_Click(object sender, EventArgs e)
        {
            counter--;

            if (counter == -1)
            {
                counter = Player.getAll().Count() - 1;
            }

            this.loadPlayer(Player.getAll().ElementAt(counter));
        }

        private void loadPlayer(Player player)
        {
            lblPlayerName.Text = player.firstName + " " + player.lastName;
            try {
                imgPlayer.Image = Image.FromFile(player.imagePath);
            } catch (Exception)
            {
                imgPlayer.Image = Properties.Resources.onbekend;
            }
            

            txtAttitude.Text = String.Empty;
            txtComment.Text = String.Empty;
            txtEffort.Text = String.Empty;
            txtShape.Text = String.Empty;
            chkPresent.Checked = false;

            currentPlayer = player;
        }

        private void chkPresent_CheckedChanged(object sender, EventArgs e)
        {
            if (chkPresent.Checked)
            {
                txtAttitude.Visible = true;
                txtComment.Visible = true;
                txtEffort.Visible = true;
                txtShape.Visible = true;
                lblAttitude.Visible = true;
                lblComment.Visible = true;
                lblEffort.Visible = true;
                lblShape.Visible = true;
                lblPoints1.Visible = true;
                lblPoints2.Visible = true;
                lblPoints3.Visible = true;
                return;
            }

            txtAttitude.Visible = false;
            txtComment.Visible = false;
            txtEffort.Visible = false;
            txtShape.Visible = false;
            lblAttitude.Visible = false;
            lblComment.Visible = false;
            lblEffort.Visible = false;
            lblShape.Visible = false;
            lblPoints1.Visible = false;
            lblPoints2.Visible = false;
            lblPoints3.Visible = false;
        }

        private void frmTrainingInput_FormClosing(object sender, FormClosingEventArgs e)
        {     
                TrainingDA.addData(currentTrainingMap);
        }

        private void btnSaveThisOne_Click(object sender, EventArgs e)
        {
            if (!chkPresent.Checked)
            {
                MessageBox.Show("Eerst de speler op aanwezig zetten als je data wilt gaan bijhouden", "Afwezig gezet", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            TrainingKey key = new TrainingKey(currentPlayer.id, dtpDate.Value.ToShortDateString());

            foreach (var pair in Training.getTrainingMap())
            {
                if (pair.Key.id == key.id)
                {
                    if (pair.Key.date == key.date)
                    {
                        MessageBox.Show("Deze speler heeft al zijn beoordeling gekregen op deze datum. Gelieve de datum te veranderen of deze speler niet meer te beoordelen", "Waarschuwing", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return;
                    }
                }
            }

            double attitude;

            try
            {
                attitude = Convert.ToDouble(txtAttitude.Text);
            } catch (Exception)
            {
                MessageBox.Show("Er is geen getal ingegeven in het veld van attitude", "Foutmelding", MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtAttitude.Text = String.Empty;
                txtAttitude.Focus();
                return;
            }

            if (attitude > 10 || attitude < 0)
            {
                MessageBox.Show("Er moet een getal tussen 0 en 10 ingegeven worden of een komma te gebruiken ipv een punt", "Foutmelding", MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtAttitude.Text = String.Empty;
                txtAttitude.Focus();
                return;
            }

            double effort;

            try
            {
                effort = Convert.ToDouble(txtEffort.Text);
            } catch (Exception)
            {
                MessageBox.Show("Er is geen getal ingegeven in het veld van inzet", "Foutmelding", MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtEffort.Text = String.Empty;
                txtEffort.Focus();
                return;
            }

            if (effort > 10 || effort < 0)
            {
                MessageBox.Show("Er moet een getal tussen 0 en 10 ingegeven worden of een komma te gebruiken ipv een punt", "Foutmelding", MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtEffort.Text = String.Empty;
                txtEffort.Focus();
                return;
            }

            double shape;

            try
            {
                shape = Convert.ToDouble(txtShape.Text);
            } catch (Exception)
            {
                MessageBox.Show("Er is geen getal ingegeven in het veld van vorm", "Foutmelding", MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtShape.Text = String.Empty;
                txtShape.Focus();
                return;
            }

            if (shape > 10 || shape < 0)
            {
                MessageBox.Show("Er moet een getal tussen 0 en 10 ingegeven worden of een komma te gebruiken ipv een punt", "Foutmelding", MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtShape.Text = String.Empty;
                txtShape.Focus();
                return;
            }

            currentTrainingMap.Add(key, new Training(attitude, effort, shape, txtComment.Text));
            Training.getTrainingMap().Add(key, new Training(attitude, effort, shape, txtComment.Text));
            
            MessageBox.Show(currentPlayer.firstName + " " + currentPlayer.lastName + " zijn training gegevens zijn succesvol opgeslagen voor " + dtpDate.Value.ToShortDateString(), "Data opgeslagen", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
    }
}
