﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using FootballCoachingPlatform.Model;
using FootballCoachingPlatform.DA;

namespace FootballCoachingPlatform
{
    public partial class frmTrainingHistory : Form
    {
        private int counter;
        private Dictionary<TrainingKey, Training> currentTrainingMap;

        public frmTrainingHistory()
        {
            InitializeComponent();

            try
            {
                imgCoach.Image = Image.FromFile(Coach.imagePath);
            }
            catch (Exception)
            {
                imgCoach.Image = Properties.Resources.onbekend;
            }

            try
            {
                imgTeam.Image = Image.FromFile(Team.imagePath);
            }
            catch (Exception)
            {
                imgTeam.Image = Properties.Resources.onbekend;
            }

            lblTeamName.Text = Team.name;
            lblUsername.Text = Coach.username;

            counter = 0;

            currentTrainingMap = new Dictionary<TrainingKey, Training>();

            foreach (var pair in Training.getTrainingMap())
            {
                if (pair.Key.date.Equals(dtpDate.Value.ToShortDateString()))
                {
                    currentTrainingMap.Add(pair.Key, pair.Value);
                }
            }

            if (currentTrainingMap.Count() == 0)
            {
                txtAttitude.Text = String.Empty;
                txtComment.Text = String.Empty;
                txtEffort.Text = String.Empty;
                txtShape.Text = String.Empty;

                lblPlayerName.Text = "Geen enkele speler heeft getraind op " + dtpDate.Value.ToShortDateString() + ".";
                imgPlayer.Image = Properties.Resources.onbekend;
                MessageBox.Show("Gelieve de datum aan te passen, want niemand heeft getraid op " + dtpDate.Value.ToShortDateString(), "Geen spelers gevonden op deze datum", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            this.loadData();

            
        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            counter++;

            if (currentTrainingMap.Count <= 1)
            {
                return;
            } else if (currentTrainingMap.Count <= counter)
            {
                counter = 0;
            }
            this.loadData();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            new frmTrainingInput().ShowDialog();
        }

        private void loadData()
        {      
            Player player = Player.getPlayerFromPlayerId(currentTrainingMap.ElementAt(counter).Key.id);
            Training training = currentTrainingMap.ElementAt(counter).Value;

            txtAttitude.Text = training.attitude.ToString();
            txtEffort.Text = training.effort.ToString();
            txtShape.Text = training.shape.ToString();
            txtComment.Text = training.comment;

            lblPlayerName.Text = player.firstName + " " + player.lastName;

            try
            {
                imgPlayer.Image = Image.FromFile(player.imagePath);
            } catch (Exception)
            {
                imgPlayer.Image = Properties.Resources.onbekend;
            }

        }

        private void btnPrevious_Click(object sender, EventArgs e)
        {
            counter--;

            if (currentTrainingMap.Count <= 1)
            {
                return;
            }
            else if (counter <= -1)
            {
                counter = currentTrainingMap.Count() - 1;
            }
            this.loadData();
        }

        private void dtpDate_ValueChanged(object sender, EventArgs e)
        {
            counter = 0;

            currentTrainingMap = new Dictionary<TrainingKey, Training>();

            foreach (var pair in Training.getTrainingMap())
            {
                if (pair.Key.date.Equals(dtpDate.Value.ToShortDateString()))
                {
                    currentTrainingMap.Add(pair.Key, pair.Value);
                }
            }

            if (currentTrainingMap.Count() == 0)
            {
                txtAttitude.Text = String.Empty;
                txtComment.Text = String.Empty;
                txtEffort.Text = String.Empty;
                txtShape.Text = String.Empty;

                lblPlayerName.Text = "Geen enkele speler heeft getraind op " + dtpDate.Value.ToShortDateString() + ".";
                imgPlayer.Image = Properties.Resources.onbekend;
                MessageBox.Show("Gelieve de datum aan te passen, want niemand heeft getraid op " + dtpDate.Value.ToShortDateString(), "Geen spelers gevonden op deze datum", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            this.loadData();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                TrainingDA.removeData(currentTrainingMap.ElementAt(counter).Key);
                currentTrainingMap.Remove(currentTrainingMap.ElementAt(counter).Key);
                MessageBox.Show("Deze trainig data is succesvol verwijderd", "data verwijderd", MessageBoxButtons.OK, MessageBoxIcon.Information);
                counter--;
            } catch (Exception exception)
            {
                MessageBox.Show("Foutmelding: " + exception);
            }
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {

            double attitude;

            try
            {
                attitude = Convert.ToDouble(txtAttitude.Text);
            }
            catch (Exception)
            {
                MessageBox.Show("Er is geen getal ingegeven in het veld van attitude", "Foutmelding", MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtAttitude.Text = String.Empty;
                txtAttitude.Focus();
                return;
            }

            if (attitude > 10 || attitude < 0)
            {
                MessageBox.Show("Er moet een getal tussen 0 en 10 ingegeven worden of een komma te gebruiken ipv een punt", "Foutmelding", MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtAttitude.Text = String.Empty;
                txtAttitude.Focus();
                return;
            }

            double effort;

            try
            {
                effort = Convert.ToDouble(txtEffort.Text);
            }
            catch (Exception)
            {
                MessageBox.Show("Er is geen getal ingegeven in het veld van inzet", "Foutmelding", MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtEffort.Text = String.Empty;
                txtEffort.Focus();
                return;
            }

            if (effort > 10 || effort < 0)
            {
                MessageBox.Show("Er moet een getal tussen 0 en 10 ingegeven worden of een komma te gebruiken ipv een punt", "Foutmelding", MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtEffort.Text = String.Empty;
                txtEffort.Focus();
                return;
            }

            double shape;

            try
            {
                shape = Convert.ToDouble(txtShape.Text);
            }
            catch (Exception)
            {
                MessageBox.Show("Er is geen getal ingegeven in het veld van vorm", "Foutmelding", MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtShape.Text = String.Empty;
                txtShape.Focus();
                return;
            }

            if (shape > 10 || shape < 0)
            {
                MessageBox.Show("Er moet een getal tussen 0 en 10 ingegeven worden of een komma te gebruiken ipv een punt", "Foutmelding", MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtShape.Text = String.Empty;
                txtShape.Focus();
                return;
            }

            try
            {
                TrainingDA.changeData(currentTrainingMap.ElementAt(counter).Key, attitude, effort, shape, txtComment.Text);
                currentTrainingMap[currentTrainingMap.ElementAt(counter).Key] = new Training(attitude, effort, shape, txtComment.Text); 
                MessageBox.Show("Deze trainig data is succesvol bijgewerkt", "data aangepast", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception exception)
            {
                MessageBox.Show("Foutmelding: " + exception);
            }
        }
    }
}
