﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using FootballCoachingPlatform.Helper;
using FootballCoachingPlatform.Model;

namespace FootballCoachingPlatform.DA
{
    public class CoachDA
    {

        private static MySqlConnection connection = Database.getConnection();

        public static void addCoach(String username, String firstname, String lastname, String mail, String password, String imagePath)
        {
            String query = "INSERT INTO footballcoachingplatform.coach (gebruikersnaam, voornaam, naam, mail, wachtwoord, afbeelding) VALUES (@gebruikersnaam, @voornaam, @naam, @mail, @wachtwoord, @afbeelding)";

            connection.Open();

            //commando maken
            MySqlCommand cmd = new MySqlCommand(query, connection);

            //parameters toevoegen
            cmd.CommandText = query;
            cmd.Parameters.AddWithValue("@gebruikersnaam", username);
            cmd.Parameters.AddWithValue("@voornaam", firstname);
            cmd.Parameters.AddWithValue("@naam", lastname);
            cmd.Parameters.AddWithValue("@mail", mail);
            cmd.Parameters.AddWithValue("@wachtwoord", password);
            cmd.Parameters.AddWithValue("@afbeelding", imagePath);

            cmd.ExecuteNonQuery();

            connection.Close();
        }

        public static bool checkIfUsernameExists(String username)
        {
            //krijg het aantal records terug waar de gebruikersnaam gelijk is aan de eerste parameter
            String query = "SELECT COUNT(*) FROM footballcoachingplatform.coach WHERE gebruikersnaam = @gebruikersnaam";

            connection.Open();

            MySqlCommand cmd = new MySqlCommand(query, connection);

            cmd.Parameters.AddWithValue("@gebruikersnaam", username);

            byte result = Convert.ToByte(cmd.ExecuteScalar());
            connection.Close();

            return (result != 0);
        }

        public static bool checkIfMailExists(String mail)
        {
            String query = "SELECT COUNT(*) FROM footballcoachingplatform.coach WHERE mail = @mail";

            connection.Open();

            MySqlCommand cmd = new MySqlCommand(query, connection);

            cmd.Parameters.AddWithValue("@mail", mail);

            byte result = Convert.ToByte(cmd.ExecuteScalar());
            connection.Close();

            return (result != 0);
        }

        public static bool login(String username, String password)
        {
            String query = "SELECT * FROM footballcoachingplatform.coach WHERE gebruikersnaam = @gebruikersnaam AND wachtwoord = @wachtwoord";

            connection.Open();

            MySqlCommand cmd = new MySqlCommand(query, connection);

            cmd.Parameters.AddWithValue("@gebruikersnaam", username);
            cmd.Parameters.AddWithValue("@wachtwoord", password);

            MySqlDataReader reader = cmd.ExecuteReader(); 

            if (!reader.HasRows)
            {
                connection.Close();
                return false;
            }

            reader.Read();
            Coach.username = reader["gebruikersnaam"].ToString();
            Coach.password = reader["wachtwoord"].ToString();
            Coach.imagePath = reader["afbeelding"].ToString();

            connection.Close();
            return true;
 
        }

        public static bool changePassword(String mail, String password)
        {
            String query = "UPDATE footballcoachingplatform.coach SET wachtwoord = @wachtwoord WHERE mail = @mail";

            connection.Open();

            MySqlCommand cmd = new MySqlCommand(query, connection);

            cmd.Parameters.AddWithValue("@mail", mail);
            cmd.Parameters.AddWithValue("@wachtwoord", password);

            connection.Close();

            return (cmd.ExecuteNonQuery() != 0);
        }

        public static String getPassword(String mail)
        {
            String query = "SELECT wachtwoord FROM footballcoachingplatform.coach WHERE mail = @mail";

            connection.Open();

            MySqlCommand cmd = new MySqlCommand(query, connection);

            cmd.Parameters.AddWithValue("@mail", mail);

            Object thePassword = cmd.ExecuteScalar();

            connection.Close();

            return thePassword.ToString();
        }
    }
}
