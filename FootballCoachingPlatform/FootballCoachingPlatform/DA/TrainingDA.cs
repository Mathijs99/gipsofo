﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FootballCoachingPlatform.Model;
using MySql.Data.MySqlClient;
using FootballCoachingPlatform.Helper;
using System.Windows.Forms;

namespace FootballCoachingPlatform.DA
{
    class TrainingDA
    {
        private static MySqlConnection connection = Database.getConnection();

        public static void addData(Dictionary<TrainingKey, Training> trainingMap)
        {

            try
            {
                connection.Open();

                /*String date = DateTime.Now.Date.ToShortDateString();

                String query = "SELECT COUNT(*) FROM footballcoaching.training WHERE datum = @date";
                MySqlCommand cmd = new MySqlCommand(query, connection);
                cmd.Parameters.AddWithValue("@date", )
                MySqlDataReader reader = cmd.ExecuteReader();
                if (reader.HasRows)
                {
                    MessageBox.Show("Deze speler heeft al zijn beoordelingen gehad op deze datum", "Foutmelding", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }*/

                //werken met id voor een speler
                String query = "INSERT INTO footballcoachingplatform.training (spelerid, datum, houding, inzet, vorm, opmerking) VALUES (@id, @date, @attitude, @effort, @shape, @comment)";

                //commando maken
                MySqlCommand cmd = new MySqlCommand(query, connection);


                foreach (var pair in trainingMap)
                {
                    TrainingKey key = pair.Key;
                    Training training = pair.Value;

                    //parameters toevoegen
                    cmd.CommandText = query;

                    cmd.Parameters.Clear();

                    cmd.Parameters.AddWithValue("@date", key.date);
                    cmd.Parameters.AddWithValue("@id", key.id);
                    cmd.Parameters.AddWithValue("@attitude", training.attitude);
                    cmd.Parameters.AddWithValue("@effort", training.effort);
                    cmd.Parameters.AddWithValue("@shape", training.shape);
                    cmd.Parameters.AddWithValue("@comment", training.comment);

                    if (!TrainingTotal.getTrainingTotalMap().Keys.Contains(key.id))
                        {
                            TrainingTotal.getTrainingTotalMap().Add(key.id, new TrainingTotal(1, training.attitude, training.effort, training.shape));
                        } else
                        {
                            TrainingTotal.getTrainingTotalMap()[key.id].attitude += training.attitude;
                            TrainingTotal.getTrainingTotalMap()[key.id].effort += training.effort;
                            TrainingTotal.getTrainingTotalMap()[key.id].shape += training.shape;
                            TrainingTotal.getTrainingTotalMap()[key.id].total += 1;
                        }

                   

                    cmd.ExecuteNonQuery();
                }

                connection.Close();

            } catch (Exception exception)
            {
                connection.Close();
                MessageBox.Show("Foutmelding" + exception);
            }
            
        }

        public static void changeData(TrainingKey key, double attitude, double effort, double shape, String comment)
        {

            String query = "UPDATE footballcoachingplatform.training SET houding = @attitude, inzet = @effort, vorm = @shape, opmerking = @comment WHERE spelerid = @playerid AND datum = @date";

            connection.Open();

            MySqlCommand cmd = new MySqlCommand(query, connection);

            cmd.Parameters.AddWithValue("@attitude", attitude);
            cmd.Parameters.AddWithValue("@effort", effort);
            cmd.Parameters.AddWithValue("@shape", shape);
            cmd.Parameters.AddWithValue("@comment", comment);

            cmd.Parameters.AddWithValue("@playerid", key.id);
            cmd.Parameters.AddWithValue("@date", key.date);

            cmd.ExecuteNonQuery();

            connection.Close();

            Training training = Training.getTrainingMap()[key];

            TrainingTotal.getTrainingTotalMap()[key.id].attitude += attitude - training.attitude;
            TrainingTotal.getTrainingTotalMap()[key.id].effort += effort - training.effort;
            TrainingTotal.getTrainingTotalMap()[key.id].shape += shape - training.shape;

            Training.getTrainingMap()[key] = new Training(attitude, effort, shape, comment);
            
        }

        public static void removeData(TrainingKey key)
        {
            String query = "DELETE FROM footballcoachingplatform.training WHERE spelerid = @playerid AND datum = @date";

            connection.Open();

            MySqlCommand cmd = new MySqlCommand(query, connection);

            cmd.Parameters.AddWithValue("@playerid", key.id);
            cmd.Parameters.AddWithValue("@date", key.date);

            cmd.ExecuteNonQuery();

            connection.Close();

            Training training = Training.getTrainingMap()[key];

            TrainingTotal.getTrainingTotalMap()[key.id].attitude -= training.attitude;
            TrainingTotal.getTrainingTotalMap()[key.id].effort -= training.effort;
            TrainingTotal.getTrainingTotalMap()[key.id].shape -= training.shape;

            Training.getTrainingMap().Remove(key);
        }
    }
}
