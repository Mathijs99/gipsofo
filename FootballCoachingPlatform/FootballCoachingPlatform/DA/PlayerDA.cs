﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using FootballCoachingPlatform.Helper;
using FootballCoachingPlatform.Model;
using System.Windows.Forms;

namespace FootballCoachingPlatform.DA
{
    public class PlayerDA
    {
        private static MySqlConnection connection = Database.getConnection();

        public static void addPlayer(String firstName, String lastName, int age, String favoritePosition, String alternativePosition, String imagePath)
        {
            String query = "INSERT INTO footballcoachingplatform.speler (coach, voornaam, naam, leeftijd, favorietepositie, nevenpositie, afbeelding) VALUES (@coach, @voornaam, @naam, @leeftijd, @favorietepositie, @nevenpositie, @afbeelding); SELECT LAST_INSERT_ID()";

                connection.Open();

                MySqlCommand cmd = new MySqlCommand(query, connection);

                cmd.Parameters.AddWithValue("@coach", Coach.username);
                cmd.Parameters.AddWithValue("@voornaam", firstName);
                cmd.Parameters.AddWithValue("@naam", lastName);
                cmd.Parameters.AddWithValue("@leeftijd", age);
                cmd.Parameters.AddWithValue("@favorietepositie", favoritePosition);
                cmd.Parameters.AddWithValue("@nevenpositie", alternativePosition);
                cmd.Parameters.AddWithValue("@afbeelding", imagePath);

                int id = Convert.ToInt32(cmd.ExecuteScalar());

                connection.Close();

                Player.getAll().Add(new Player(id, firstName, lastName, age, favoritePosition, alternativePosition, imagePath));
        }



        public static void removePlayer(Player player)
        {
            String query = "DELETE FROM footballcoachingplatform.speler WHERE spelerid = @playerid";

                connection.Open();

                MySqlCommand cmd = new MySqlCommand(query, connection);

                cmd.Parameters.AddWithValue("@playerid", player.id);

                cmd.ExecuteNonQuery();

                connection.Close();

                Player.getAll().Remove(player);


        }

        public static void changePlayerData(Player player, String firstName, String lastName, int age, String favoritePosition, String alternativePosition, String imagePath)
        {
            String query = "UPDATE footballcoachingplatform.speler SET voornaam = @voornaam, naam = @naam, leeftijd = @leeftijd, favorietePositie = @favorietePositie, nevenPositie = @nevenPositie, afbeelding = @afbeelding WHERE spelerid = @playerid";

                connection.Open();

                MySqlCommand cmd = new MySqlCommand(query, connection);

                cmd.Parameters.AddWithValue("@voornaam", firstName);
                cmd.Parameters.AddWithValue("@naam", lastName);
                cmd.Parameters.AddWithValue("@leeftijd", age);
                cmd.Parameters.AddWithValue("@favorietePositie", favoritePosition);
                cmd.Parameters.AddWithValue("@nevenPositie", alternativePosition);
                cmd.Parameters.AddWithValue("@afbeelding", imagePath);

                cmd.Parameters.AddWithValue("@playerid", player.id);

                cmd.ExecuteNonQuery();

                connection.Close();
            

            player.firstName = firstName;
            player.lastName = lastName;
            player.age = age;
            player.favoritePosition = favoritePosition;
            player.alternativePosition = alternativePosition;
            player.imagePath = imagePath;
        }
    }
}
