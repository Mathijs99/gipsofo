﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FootballCoachingPlatform.Model;
using MySql.Data.MySqlClient;
using FootballCoachingPlatform.Helper;

namespace FootballCoachingPlatform.DA
{
    public class MatchDA
    {
        private static MySqlConnection connection = Database.getConnection();

        public static void addMatchData(List<MatchData> matchDataList, String date)
        {
            String query = "INSERT INTO footballcoachingplatform.match (datum, spelerid, positie) VALUES (@date, @playerid, @position)";

            connection.Open();

            MySqlCommand cmd = new MySqlCommand(query, connection);


            foreach (MatchData match in matchDataList)
            {
                cmd.Parameters.Clear();

                cmd.Parameters.AddWithValue("@date", date);
                cmd.Parameters.AddWithValue("@playerid", match.playerid);
                cmd.Parameters.AddWithValue("@position", match.position);

                cmd.ExecuteNonQuery();
            }

            connection.Close();
        }
    }
}
