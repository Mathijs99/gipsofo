﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using FootballCoachingPlatform.Helper;
using FootballCoachingPlatform.Model;

namespace FootballCoachingPlatform.DA
{
    public class TeamDA
    {
        private static MySqlConnection connection = Database.getConnection();

        public static void addTeam(String name, String imagePath, String coach)
        {
            String query = "INSERT INTO footballcoachingplatform.ploeg (naam, afbeelding, coach) VALUES (@naam, @afbeelding, @coach)";

            connection.Open();

            MySqlCommand cmd = new MySqlCommand(query, connection);

            cmd.Parameters.AddWithValue("@naam", name);
            cmd.Parameters.AddWithValue("@afbeelding", imagePath);
            cmd.Parameters.AddWithValue("@coach", coach);

            cmd.ExecuteNonQuery();

            connection.Close();

            Team.name = name;
            Team.imagePath = imagePath;
        }
    }
}
