﻿namespace FootballCoachingPlatform
{
    partial class frmMyFootballTeam
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lblTeamName = new System.Windows.Forms.Label();
            this.lblUsername = new System.Windows.Forms.Label();
            this.imgMatchday = new System.Windows.Forms.PictureBox();
            this.imgTraining = new System.Windows.Forms.PictureBox();
            this.imgMyTeam = new System.Windows.Forms.PictureBox();
            this.imgTeamLogo = new System.Windows.Forms.PictureBox();
            this.imgCoach = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.imgMatchday)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgTraining)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgMyTeam)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgTeamLogo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgCoach)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(577, 159);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(115, 25);
            this.label1.TabIndex = 0;
            this.label1.Text = "Matchdag";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(45, 159);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(114, 25);
            this.label2.TabIndex = 1;
            this.label2.Text = "Mijn team";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(323, 159);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(98, 25);
            this.label3.TabIndex = 2;
            this.label3.Text = "Training";
            // 
            // lblTeamName
            // 
            this.lblTeamName.AutoSize = true;
            this.lblTeamName.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTeamName.Location = new System.Drawing.Point(160, 12);
            this.lblTeamName.Name = "lblTeamName";
            this.lblTeamName.Size = new System.Drawing.Size(195, 24);
            this.lblTeamName.TabIndex = 4;
            this.lblTeamName.Text = "<Naam voetbalploeg>";
            // 
            // lblUsername
            // 
            this.lblUsername.AutoSize = true;
            this.lblUsername.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUsername.Location = new System.Drawing.Point(160, 68);
            this.lblUsername.Name = "lblUsername";
            this.lblUsername.Size = new System.Drawing.Size(171, 24);
            this.lblUsername.TabIndex = 5;
            this.lblUsername.Text = "<Gebruikersnaam>";
            // 
            // imgMatchday
            // 
            this.imgMatchday.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.imgMatchday.Image = global::FootballCoachingPlatform.Properties.Resources.match;
            this.imgMatchday.Location = new System.Drawing.Point(560, 234);
            this.imgMatchday.Name = "imgMatchday";
            this.imgMatchday.Size = new System.Drawing.Size(159, 146);
            this.imgMatchday.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.imgMatchday.TabIndex = 8;
            this.imgMatchday.TabStop = false;
            this.imgMatchday.Click += new System.EventHandler(this.imgMatchday_Click);
            // 
            // imgTraining
            // 
            this.imgTraining.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.imgTraining.Image = global::FootballCoachingPlatform.Properties.Resources.training;
            this.imgTraining.Location = new System.Drawing.Point(299, 234);
            this.imgTraining.Name = "imgTraining";
            this.imgTraining.Size = new System.Drawing.Size(159, 146);
            this.imgTraining.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.imgTraining.TabIndex = 7;
            this.imgTraining.TabStop = false;
            this.imgTraining.Click += new System.EventHandler(this.imgTraining_Click);
            // 
            // imgMyTeam
            // 
            this.imgMyTeam.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.imgMyTeam.Image = global::FootballCoachingPlatform.Properties.Resources.myteam;
            this.imgMyTeam.Location = new System.Drawing.Point(24, 234);
            this.imgMyTeam.Name = "imgMyTeam";
            this.imgMyTeam.Size = new System.Drawing.Size(159, 146);
            this.imgMyTeam.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.imgMyTeam.TabIndex = 6;
            this.imgMyTeam.TabStop = false;
            this.imgMyTeam.Click += new System.EventHandler(this.imgMyTeam_Click);
            // 
            // imgTeamLogo
            // 
            this.imgTeamLogo.Location = new System.Drawing.Point(609, 12);
            this.imgTeamLogo.Name = "imgTeamLogo";
            this.imgTeamLogo.Size = new System.Drawing.Size(110, 80);
            this.imgTeamLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.imgTeamLogo.TabIndex = 3;
            this.imgTeamLogo.TabStop = false;
            // 
            // imgCoach
            // 
            this.imgCoach.Location = new System.Drawing.Point(24, 12);
            this.imgCoach.Name = "imgCoach";
            this.imgCoach.Size = new System.Drawing.Size(110, 80);
            this.imgCoach.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.imgCoach.TabIndex = 9;
            this.imgCoach.TabStop = false;
            // 
            // frmMyFootballTeam
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(763, 450);
            this.Controls.Add(this.imgCoach);
            this.Controls.Add(this.imgMatchday);
            this.Controls.Add(this.imgTraining);
            this.Controls.Add(this.imgMyTeam);
            this.Controls.Add(this.lblUsername);
            this.Controls.Add(this.lblTeamName);
            this.Controls.Add(this.imgTeamLogo);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "frmMyFootballTeam";
            this.Text = "frmMyFootballTeam";
            ((System.ComponentModel.ISupportInitialize)(this.imgMatchday)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgTraining)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgMyTeam)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgTeamLogo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgCoach)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.PictureBox imgTeamLogo;
        private System.Windows.Forms.Label lblTeamName;
        private System.Windows.Forms.Label lblUsername;
        private System.Windows.Forms.PictureBox imgMyTeam;
        private System.Windows.Forms.PictureBox imgTraining;
        private System.Windows.Forms.PictureBox imgMatchday;
        private System.Windows.Forms.PictureBox imgCoach;
    }
}