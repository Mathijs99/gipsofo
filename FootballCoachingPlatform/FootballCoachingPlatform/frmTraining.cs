﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using FootballCoachingPlatform.Model;

namespace FootballCoachingPlatform
{
    public partial class frmTraining : Form
    {
        public frmTraining()
        {
            InitializeComponent();
        }

        private void btnInput_Click(object sender, EventArgs e)
        {
            if (Player.getAll().Count() == 0)
            {
                MessageBox.Show("U heeft nog geen spelers in uw team. Gelieve eerst eerst spelers toe te voegen via \'Mijn Team\'", "Geen spelers", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                this.Hide();
                new frmTrainingInput();
                new frmAddPlayer().ShowDialog();
                this.Close();
                return;
            }

            new frmTrainingInput().ShowDialog();
        }

        private void btnHistory_Click(object sender, EventArgs e)
        {
            new frmTrainingHistory().ShowDialog();
        }
    }
}
