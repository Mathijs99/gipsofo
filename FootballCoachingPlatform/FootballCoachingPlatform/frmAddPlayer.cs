﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using FootballCoachingPlatform.DA;
using FootballCoachingPlatform.Model;
using FootballCoachingPlatform.Helper;

namespace FootballCoachingPlatform
{
    public partial class frmAddPlayer : Form
    {
        String imagePath = String.Empty;

        public frmAddPlayer()
        {
            InitializeComponent();

            cboFavoritePosition.Items.Add("Aanvaller");
            cboFavoritePosition.Items.Add("Middenvelder");
            cboFavoritePosition.Items.Add("Verdediger");
            cboFavoritePosition.Items.Add("Keeper");
            cboAlternativePosition.Items.Add("Aanvaller");
            cboAlternativePosition.Items.Add("Middenvelder");
            cboAlternativePosition.Items.Add("Verdediger");
            cboAlternativePosition.Items.Add("Keeper");
            cboAlternativePosition.Items.Add("Geen");
        }

        private void btnUpload_Click(object sender, EventArgs e)
        {
            imagePath = ImageManager.handle(imgPlayer);
        }

        private void btnAddPlayer_Click(object sender, EventArgs e)
        {
            String firstname = txtFirstname.Text;
            String lastname = txtLastname.Text;
            int age = this.calculateAge(dtpDateOfBirth.Value);
            String favoritePosition = cboFavoritePosition.Text;
            String alternativePosition = cboAlternativePosition.Text;

            if (!checkIfEmpty(firstname, lastname, favoritePosition, alternativePosition))
            {
                if (alternativePosition == favoritePosition)
                {
                    MessageBox.Show("Favoriete positie en nevenpositie mogen niet hetzelfde zijn", "Foutmelding", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    cboAlternativePosition.SelectedIndex = 0;
                    cboFavoritePosition.SelectedIndex = 0;
                    cboAlternativePosition.Focus();
                    return;
                }

                try
                {
                    PlayerDA.addPlayer(firstname, lastname, age, favoritePosition, alternativePosition, imagePath);
                } catch (Exception exception)
                {
                    MessageBox.Show("Foutmelding" + exception);
                    return;
                }
                
                MessageBox.Show("Deze speler is succesvol toegevoegd tot de database", "Speler toegevoegd", MessageBoxButtons.OK, MessageBoxIcon.Information);

                txtFirstname.Text = String.Empty;
                txtLastname.Text = String.Empty;
                cboAlternativePosition.SelectedIndex = 0;
                cboFavoritePosition.SelectedIndex = 0;
                imgPlayer.Image = Properties.Resources.onbekend;
                
            }
        }

        private bool checkIfEmpty(String firstname, String lastname, String favoritePosition, String alternativePosition)
        {
            if (firstname == String.Empty)
            {
                MessageBox.Show("Voornaam niet ingvuld!", "Voornaam", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtFirstname.Focus();
                return true;
            }

            if (lastname == String.Empty)
            {
                MessageBox.Show("Achternaam niet ingvuld!", "Achternaam", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtLastname.Focus();
                return true;
            }

            if (favoritePosition == String.Empty)
            {
                MessageBox.Show("Favoritie positie niet gekozen!", "Favoriete positie", MessageBoxButtons.OK, MessageBoxIcon.Information);
                cboFavoritePosition.Focus();
                return true;
            }

            if (alternativePosition == String.Empty)
            {
                MessageBox.Show("Neven positie niet ingvuld!", "Voornaam", MessageBoxButtons.OK, MessageBoxIcon.Information);
                cboAlternativePosition.Focus();
                return true;
            }

            return false;
        }

        private int calculateAge(DateTime date)
        {
            int age = DateTime.Today.Year - date.Year;

            if (date.DayOfYear > DateTime.Today.DayOfYear)
            {
                age -= 1;
            }

            return age;
        }
    }
}
